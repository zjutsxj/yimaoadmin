<?php
// 参数变量
Route::pattern([
    'page' => '\d+',
    'id' => '\d+',
    'cateid' => '\d+',
    'filename' => '[a-z0-9\-]+',
    'catename' => '[a-z0-9\-]+',
]);

// admin后台
Route::rule('admin/:controller/:action', 'admin/:controller/:action');
Route::rule('admin/:controller', 'admin/:controller/index');
Route::rule('admin', 'admin/Index/index');

// api接口
Route::rule('/api/:controller/:action', 'api/:controller/:action');
Route::rule('/api/:controller', 'api/:controller/index');
Route::rule('/api', 'api/Index/index');