<?php 

// 英文网站
Route::rule('en/:action/<catename>-<cateid>-p<page>', 'index/En/:action');
Route::rule('en/:action/<catename>-<cateid>', 'index/En/:action');
Route::rule('en/:action-p<page>', 'index/En/:action');
Route::rule('en/:action/<id>', 'index/En/:action');
Route::rule('en/:action', 'index/En/:action');
Route::rule('en/', 'index/En/index');

// 网站内容
Route::rule(':action/<catename>-<cateid>-p<page>', 'index/Cn/:action');
Route::rule(':action/<catename>-<cateid>', 'index/Cn/:action');
Route::rule(':action/<id>', 'index/Cn/:action');
Route::rule(':action-p<page>', 'index/Cn/:action');
Route::rule(':action', 'index/Cn/:action');
Route::rule('', 'index/Cn/index');

