<?php
namespace app\index\controller;

use app\common\controller\HomeBaseController;
use think\facade\Env;

class CnController extends HomeBaseController
{
    public function index()
    {
        return $this->fetch();
    }

    public function _empty()
    {
        // 项目根目录
        $tplpath = Env::get('root_path');
        $tplpath .= 'public/';

        // 模板地址
        $tplpath .= config('template.view_path');
        $tplpath .= $this->request->controller().'/';
        $tplpath .= $this->request->action();

        // 分类名称 / 分类ID / 文章ID
        $catename = $this->request->param('catename', '');
        $cateid = $this->request->param('cateid', 0);
        $articleid = $this->request->param('id', 0);
        if ($catename != '' && $cateid > 0) {
            $tplpath .= '_cate';
        }

        // 详情
        if ($articleid > 0) {
            $tplpath .= '_details';
        }

        $tplpath .= '.html';

        // 模板路径
        $tplpath = strtolower($tplpath);

        // 判断模板文件
        if (file_exists($tplpath)) {
            return $this->fetch($tplpath);
        }

        self::page_404('Cn/_empty <br> 模板文件不存在 <br>' . $tplpath);
    }
}
