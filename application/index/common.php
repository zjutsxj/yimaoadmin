<?php
// +----------------------------------------------------------------------
// | @Author Yimao <zjutsxj@qq.com>
// | @DateTime 2020-06-23
// +----------------------------------------------------------------------

/**
 * [sp_page 单页内容]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
function sp_page($id = 0)
{
    $id = chk_num($id);
    $result = db('site_column')->find($id);

    // 自定义变量
    if($result['column_field'] == 1){
        $fields = 'content_paramid, content_articleid, content_type, content_value, content_name';
        $where  = 'content_siteid = ' . $result['column_siteid'] . ' and content_columnid = ' . $result['column_id'];
        $pclist = db('site_content')->where($where)->column($fields, 'content_id');
        // 赋值
        foreach ($pclist as $k => $v) {
            $value = $v['content_value'];
            if ($v['content_type'] == 'checkbox') {
                $result['param_' . $v['content_paramid']] = implode(',', unserialize($value));
            } else {
                $result['param_' . $v['content_paramid']] = $value;
            }
            unset($pclist[$k]);
        }
    }

    return $result;
}

/**
 * [sp_desc 获取文本内容]
 * @param  integer $id [栏目ID]
 * @return [type]            [返回字符串]
 */
function sp_desc($id = 0)
{
    $result = sp_page($id);
    $details = ($result) ? $result['column_details'] : 'no desc';
    return $details;
}

/**
 * [sp_column 站点栏目]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
function sp_column($id = 0)
{
    return sp_page($id);
}

/**
 * [sp_column_cate 栏目分类]
 * @param  integer $id [分类id ]
 * @return [type]      [返回分类内容]
 */
function sp_column_cate($cateid = 0)
{
    $cateid = ($cateid <= 0) ? request()->param('cateid', 0) : 0;
    $cateid = chk_num($cateid, 'cateid');
    $result = db('wscate')->find($cateid);
    return $result;
}

/**
 * [sp_cate 获取分类]
 * @param  integer $columnid    [栏目id]
 * @param  integer $parentid    [父ID]
 * @param  integer $number      [获取数量]
 * @param  array $options       [其它参数]
 * @return [type]               [返回列表]
 */
function sp_cate($columnid = 0, $parentid = '', $number = 0, $options = [])
{
    // 搜索条件
    $where = 'wscate_columnid = ' . $columnid;

    // 获取几级分类，默认全部
    if ($parentid !== '') {
        $where = $where . ' and wscate_pid = ' . $parentid;
    }

    // 自定义搜索条件
    if (isset($options['where'])) {
        $where = $options['where'];
    }

    // 排序方式
    $order = isset($options['order']) ? $options['order'] : 'wscate_sort asc';

    if ($number > 0) {
        return db('wscate')->where($where)->field('wscate_details', true)->order($order)->limit($number)->select();
    } else {
        return db('wscate')->where($where)->field('wscate_details', true)->order($order)->select();
    }
}

/**
 * [sp_article 文章列表]
 * @param  integer $columnid [栏目id]
 * @param  integer $cateid   [分类id]
 * @param  integer $number   [获取数量]
 * @param  array   $options  [其它参数]
 * @return [type]            [列表]
 */
function sp_article($columnid = 0, $cateid = 0, $number = 0, $options = [])
{
    // 搜索条件
    $where = 'wsarticle_columnid = ' . $columnid;
    if ($cateid > 0) {
        $where .= ' and find_in_set(' . $cateid . ', wsarticle_cateid)';
    }

    // 自定义搜索条件
    if (isset($options['where'])) {
        $where = $options['where'];
    }

    // 分页数量 | 获取数量
    $pagesize = $number;

    // 排序方式
    $order = isset($options['order']) ? $options['order'] : '';

    // 分页url
    $pageurl = isset($options['pageurl']) ? $options['pageurl'] : '';

    // 分页参数
    $query = isset($options['query']) ? $options['query'] : [];

    // 是否读取分类内容
    $cate = isset($options['cate']) ? $options['cate'] : false;

    // 是否读取自定义字段
    $param = isset($options['param']) ? $options['param'] : false;

    // 当前配置
    $my_options = ['cate' => $cate, 'param' => $param, 'query' => $query];

    return sp_list($where, $order, $pagesize, $pageurl, $my_options);
}

/**
 * [sp_search 文章搜索]
 * @Author   Yimao      [zjutsxj@qq.com]
 * @DateTime 2021-07-01
 * @param    array      $options         [description]
 * @return   [type]                      [description]
 */
function sp_search($options = []){
    // 搜索条件
    $where = isset($options['where']) ? $options['where'] : '';
    // 排序
    $order = isset($options['order']) ? $options['order'] : 'wsarticle_sort desc, wsarticle_id desc';
    // 每页数量
    $pagesize = isset($options['pagesize']) ? $options['pagesize'] : 20;
    // 分页地址
    $pageurl = isset($options['pageurl']) ? $options['pageurl'] : '';
    // 分页参数 数组类型
    //$query = isset($options['query']) ? $options['query'] : [];
    // 是否显示分类
    //$cate = isset($options['cate']) ? $options['cate'] : false;
    // 是否显示自定义参数
    //$param = isset($options['param']) ? $options['param'] : false;
    return sp_list($where, $order, $pagesize, $pageurl, $options);
}

/**
 * [sp_list 获取文章]
 * @param  string  $where   [搜索条件]
 * @param  string  $order   [排序字段]
 * @param  integer $pagesize[每页数量 | 获取数量]
 * @param  string  $pageurl [分页url]
 * @param  string  $options [自定义字段]
 * @return [type]           [返回列表]
 */
function sp_list($where = '', $order = '', $pagesize = 0, $pageurl = '', $options = [])
{
    // 搜索条件
    if ($where == '') {
        abort(404, '提示：(sp_list) 必须设置搜索条件!');
    }

    // 字段排序
    if ($order == '') {
        $order = 'wsarticle_sort desc, wsarticle_id desc';
    }

    // 文章列表
    $list = array();

    // 分页 pageurl | 没有设置 pageurl 数据不分页
    if ($pageurl == '') {
        // 获取数量
        if ($pagesize <= 0) {
            $list = db('wsarticle')->where($where)->field('wsarticle_details', true)->order($order)->select();
        } else {
            $list = db('wsarticle')->where($where)->field('wsarticle_details', true)->order($order)->limit($pagesize)->select();
        }
    } else {
        // 设置每页 20 条
        if ($pagesize <= 0) {
            $pagesize = 20;
        }

        // 获取分页数据
        $page_options = [
            'path'  => url(str_replace('[page]', '[PAGE]', $pageurl)), 
            'query' => $options['query']
        ];
        $list = db('wsarticle')->where($where)->field('wsarticle_details', true)->order($order)->paginate($pagesize, false, $page_options);
    }

    // 分类/自定义参数
    $cate = isset($options['cate']) ? $options['cate'] : false;
    $param = isset($options['param']) ? $options['param'] : false;

    if (!$cate && !$param) {
        return $list;
    }

    // 分类显示  自定义字段显示
    $mylist = gettype($list) == 'object' ? $list->all() : $list;

    if ($cate) {
        $cateid = array_column($mylist, 'wsarticle_cateid');
        $cateid = implode(',', $cateid);
        $cateid = explode(',', $cateid);
        $cateid = array_unique($cateid);

        //dump($cateid);
        $fields = 'wscate_id, wscate_name, wscate_page, wscate_image';
        $ctlist = db('wscate')->where('wscate_id', 'in', $cateid)->column($fields, 'wscate_id');
    }

    if ($param) {
        $artid = array_column($mylist, 'wsarticle_id');
        $artid = array_unique($artid);
        $fields = 'content_paramid, content_articleid, content_type, content_value, content_name';
        $pclist = db('site_content')->where('content_articleid', 'in', $artid)->column($fields, 'content_id');
    }
    //dump($ctlist);
    foreach ($mylist as $key => $items) {
        $myitem = $items;

        // 分类内容
        if ($cate && $ctlist) {
            // 多个分类时 取最后一个分类
            $cateid = $items['wsarticle_cateid'];
            if (stripos($cateid, ',') !== false) {
                $cateid = substr(strrchr($cateid, ','), 1);
            }
            if (isset($ctlist[$cateid])) {
                $myitem['wscate'] = $ctlist[$cateid];
            } else {
                $myitem['wscate'] = array();
            }
        }
        // 自定义字段
        if ($param && $pclist) {
            $artid = $items['wsarticle_id'];
            foreach ($pclist as $k => $v) {
                if ($v['content_articleid'] == $artid) {
                    $value = $v['content_value'];
                    if ($v['content_type'] == 'checkbox') {
                        $myitem['param_' . $v['content_paramid']] = implode(',', unserialize($value));
                    } else {
                        $myitem['param_' . $v['content_paramid']] = $value;
                    }
                    unset($pclist[$k]);
                }
            }
        }

        // 赋值给数据对象
        $list[$key] = $myitem;
    }

    return $list;
}

/**
 * [sp_details 文章内容]
 * @param  integer $id    [文章 id]
 * @param  boolean $param [默认不加载 自定义字段]
 * @return [type]         [description]
 */
function sp_details($id = 0, $param = false, $cate = false)
{
    $id = ($id <= 0) ? request()->param('id', 0) : $id;

    $id = chk_num($id);

    $result = db('wsarticle')->find($id);

    // 更新浏览量
    db('wsarticle')->where('wsarticle_id', $id)->setInc('wsarticle_views');

    // 自定义字段
    if ($param) {
        $fields = 'content_articleid, content_paramid, content_type, content_name, content_value';
        $list = db('site_content')->where('content_articleid=' . $id)->column($fields, 'content_paramid');
        foreach ($list as $key => $value) {
            $paramid = $value['content_paramid'];
            if ($value['content_type'] == 'checkbox') {
                $result['param_' . $paramid] = implode(',', unserialize($value['content_value']));
            } else {
                $result['param_' . $paramid] = $value['content_value'];
            }
        }
    }

    // 分类信息
    if ($cate) {
        $cateid = $result['wsarticle_cateid'];
        if (stripos($cateid, ',') !== false) {
            $cateid = substr(strrchr($cateid, ','), 1);
        }
        $wscate = db('wscate')->find($cateid);
        $result['wscate'] = $wscate;
    }

    return $result;
}

/**
 * 面包屑导航 分类
 *
 * @Author Yimao [zjutsxj@qq.com]
 * @DateTime 2020-07-17
 * @param [type] $catepath 分类id
 * @return void
 */
function sp_breadcrumb($catepath = '0')
{
    $where = 'wscate_id in (' . $catepath . ')';
    $order = 'wscate_sort asc';
    $list = db('wscate')->where($where)->field('wscate_details', true)->order($order)->select();
    return $list;
}
/**
 * 面包屑导航 详情页
 *
 * @Author Yimao [zjutsxj@qq.com]
 * @DateTime 2020-07-17
 * @param [type] $cateid 分类id
 * @return void
 */
function sp_breadcrumb_details($cateid = '0')
{
    // 处理分类ID
    if (strrchr($cateid, ',')) {
        $cateid = substr(strrchr($cateid, ','), 1);
    }
    if ($cateid <= 0) {
        return false;
    }
    // 父分类ID
    $catepath = db('wscate')->where('wscate_id = ' . $cateid)->value('wscate_path');
    $catepath .= ',' . $cateid;
    // 搜索条件
    $where = 'wscate_id in (' . $catepath . ')';
    $order = 'wscate_sort asc';
    $list = db('wscate')->where($where)->field('wscate_details', true)->order($order)->select();
    return $list;
}
/**
 * 上一条数据
 *
 * @Author Yimao [zjutsxj@qq.com]
 * @DateTime 2020-07-17
 * @param [type] $columnid
 * @param [type] $id
 * @return void
 */
function sp_prev($columnid, $id)
{
    $where = 'wsarticle_columnid = ' . $columnid . ' and wsarticle_id > ' . $id;
    $order = 'wsarticle_id asc';
    $field = 'wsarticle_id,wsarticle_title,wsarticle_desc,wsarticle_page,wsarticle_image';
    $result = db('wsarticle')->where($where)->field($field)->order($order)->find();
    return $result;
}

/**
 * 下一条数据
 *
 * @Author Yimao [zjutsxj@qq.com]
 * @DateTime 2020-07-17
 * @param [type] $columnid
 * @param [type] $id
 * @return void
 */
function sp_next($columnid, $id)
{
    $where = 'wsarticle_columnid = ' . $columnid . ' and wsarticle_id < ' . $id;
    $order = 'wsarticle_id desc';
    $field = 'wsarticle_id,wsarticle_title,wsarticle_desc,wsarticle_page,wsarticle_image';
    $result = db('wsarticle')->where($where)->field($field)->order($order)->find();
    return $result;
}

/**
 * [chk_num 验证数据]
 * @param  [type] $param [description]
 * @return [type]        [description]
 */
function chk_num($value = 0, $name = 'id')
{
    $value = intval($value);
    if ($value <= 0) {
        abort(404, '提示: ' . $name . ' 必须为有效值!');
    }
    return $value;
}

/**
 * [sf_param 获取自定义字段内容]
 * @param  [type] $param [description]
 * @param  [type] $field [description]
 * @return [type]        [description]
 */
function sf_param($param = [], $field = '')
{
    if (count($param) <= 0) {
        return '';
    }

    if ($field == '') {
        return '';
    }

    if (!isset($param[$field])) {
        return '';
    }

    return $param[$field];
}

/**
 * 获取分类信息
 * @param array $cate
 * @param string $field
 * @return void
 */
function sf_cate($cate = [], $field = '')
{
    if (count($cate) <= 0) {
        return '';
    }

    if ($field == '') {
        return '';
    }

    if (!isset($cate[$field])) {
        return '';
    }

    return $cate[$field];
}

/**
 * 根据提供的数据 生成 url 地址
 * 只支持 wsarticle, wscate 表
 * 具体请参考路由定义的规则 application/route/route.php
 * @Author   Yimao      [zjutsxj@qq.com]
 * @DateTime 2021-06-05
 * @param    string     $url             [地址前缀]
 * @param    array      $item            [文章数据/分类数据]
 * @param    boolean    $page            [生成分类分页]
 * @return   [type]                      [description]
 */
function sf_url($url = 'news', $item = [], $page = false)
{
    $myurl = '/' . $url;
    // 文章详情地址
    if(isset($item['wscate_id'])){
        if($page) {
            return $myurl . '/' . $item['wscate_page'] . '-' . $item['wscate_id'] . '-p[page]';
        } else {
            return url($myurl . '/' . $item['wscate_page'] . '-' . $item['wscate_id']);
        }
    }
    // 分类列表地址
    if(isset($item['wsarticle_id'])){
        return url($myurl . '/' . $item['wsarticle_id']);
    }
    // 栏目分页
    if($page){
        return $myurl . '-p[page]';
    }
}

/**
 * 文件下载
 * @Author   Yimao      [zjutsxj@qq.com]
 * @DateTime 2021-06-05
 * @param    [type]     $id              [description]
 * @return   [type]                      [description]
 */
function sf_download($id)
{
    return url('/api/download') . '?id=' . $id;
}

/**
 * 获取图片 地址
 * @param [type] $image
 * @param integer $num
 * @return void
 */
function sf_getimg($image, $num = 1, $key = 'file_path')
{
    if ($image == '' && $image != null && $image != 'a:0:{}') {
        return '';
    }

    $num = ($num <= 0) ? 1 : $num;
    $arg = unserialize($image);

    if (!is_array($arg)) {
        return '';
    }

    if (count($arg) <= 0) {
        return '';
    }

    // 去除数组键值
    $arg = array_values($arg);
    if (!isset($arg[$num - 1][$key])) {
        return '';
    }
    return $arg[$num - 1][$key];
}

/**
 * 获取图片列表
 * @param [type] $image
 * @return void
 */
function sf_imgslist($image)
{
    if ($image == '') {
        return [];
    }

    $arg = unserialize($image);
    if (!is_array($arg)) {
        return [];
    }

    if (count($arg) <= 0) {
        return [];
    }
    return $arg;
}


/**
 * 获取文件 地址
 * @param [type] $file
 * @param integer $num
 * @return void
 */
function sf_getfile($file, $num = 1, $key = 'file_path')
{
    return sf_getimg($file, $num, $key);
}

/**
 * 获取文件列表
 * @param [type] $image
 * @return void
 */
function sf_fileslist($file)
{
    return sf_imgslist($file);
}



/**
 * 判断前台用户是否登录
 * @return boolean
 */
function sf_is_user_login()
{
    $sessionUser = session('user');
    return !empty($sessionUser);
}

/**
 * 获取当前登录的前台用户的信息，未登录时，返回false
 * @return array|boolean
 */
function sf_get_current_user()
{
    $sessionUser = session('user');
    if (!empty($sessionUser)) {
        unset($sessionUser['user_pass']); // 销毁敏感数据
        return $sessionUser;
    } else {
        return false;
    }
}

/**
 * 更新当前登录前台用户的信息
 * @param array $user 前台用户的信息
 */
function sf_update_current_user($user)
{
    session('user', $user);
}

/**
 * 获取当前登录前台用户id
 * @return int
 */
function sf_get_current_user_id()
{
    $sessionUserId = session('user.id');
    if (empty($sessionUserId)) {
        return 0;
    }

    return $sessionUserId;
}
