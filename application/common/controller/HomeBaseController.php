<?php
namespace app\common\controller;

use app\common\controller\BaseController;
use think\Controller;

class HomeBaseController extends BaseController
{
    protected function initialize()
    {
        // 初始化站点数据
        $data = self::initData();
        $this->assign($data);
    }

    // 初始化当前数据
    private function initData()
    {
        $sites = self::getSites();
        if (count($sites) == 0) {
            exit('未设置站点！');
        }
        $site = $defs = []; // default site 默认站点
        $lang = strtolower($this->request->controller());
        foreach ($sites as $key => $val) {
            // 默认站点
            if ($val['site_default'] == '1') {
                $defs = self::getSite($val['site_id']);
            }
            // 当前站点
            if ($val['site_mark'] == $lang) {
                $site = self::getSite($val['site_id']);
                break;
            }
        }

        if (empty($site)) {
            $site = $defs;
        }

        $data = [
            'site_id' => $site['site_id'],
            'site_home' => $site['site_home'],
            'site_title' => $site['site_title'],
            'site_keywords' => $site['site_keywords'],
            'site_description' => $site['site_description'],
            'site_jscode' => $site['site_jscode'],
        ];
        // 自定义参数
        $param = self::getSiteContent($site['site_id']);

        foreach ($param as $key => $val) {
            $name = 'site_param_' . $val['content_paramid'];
            $value = $val['content_value'];
            if ($val['content_type'] == 'checkbox') {
                $value = unserialize($value);
                if(is_array($value)){
                    $value = implode(',', $value);
                }
            }
            $data[$name] = $value;
        }
        
        return $data;
    }

    protected function page_404($str = '')
    {
        header('HTTP/1.1 404 Not Found');
        $html = <<<CONTENT_HEADER
<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <meta charset="UTF-8">
    <title>404页面不存在</title>
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="stylesheet" type="text/css" href="/static/css/zui.min.css">
    <style type="text/css">
        body{font-size: 14px;background-color: #039BE5;}
        .modal-backdrop.in {opacity: 0;}
        .modal-header {padding: 10px;}
        .modal-footer{padding: 10px;}
        .content{font-size: 16px;}
        @media (min-width: 768px) {.modal-dialog {width: 500px;}}
    </style>
</head>
<body>
<div class="modal" data-backdrop="static" data-keyboard="false" id="message">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">提示信息</h4></div>
            <div class="modal-body">$str</div>
            <div class="modal-footer text-right"> <a href="/" class="btn btn-primary">返回首页</a></div>
        </div>
    </div>
</div>
<script src="/static/lib/jquery/jquery.js"></script>
<script src="/static/js/zui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#message").modal('show');
    });
</script>
</body>
</html>
CONTENT_HEADER;
        echo $html;

        //dump($this->request);
    }

}
