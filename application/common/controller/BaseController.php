<?php
namespace app\common\controller;

use app\common\model\SiteColumnModel;
use app\common\model\SiteContentModel;
use app\common\model\SiteModel;
use think\Controller;

/**
 *
 */
class BaseController extends Controller
{
    /**
     * 站点列表
     */
    protected function getSites()
    {
        // 存在缓存，直接返回缓存数据
        if (cache('?cache_site_list')) {
            return cache('cache_site_list');
        }

        $sites = SiteModel::order('site_sort asc')->column('site_id, site_default, site_name, site_mark');

        //缓存数据
        cache('cache_site_list', $sites);
        return $sites;
    }

    protected function getSite($siteid)
    {
        // 存在缓存，直接返回缓存数据
        if (cache('?cache_site_' . $siteid)) {
            return cache('cache_site_' . $siteid);
        }

        $site = SiteModel::get($siteid);

        // 缓存数据
        cache('cache_site_' . $siteid, $site);

        return $site;
    }

    /**
     * 栏目列表
     */
    protected function getColumns($siteid)
    {
        // 存在缓存，直接返回缓存数据
        if (cache('?cache_columns_' . $siteid)) {
            return cache('cache_columns_' . $siteid);
        }

        $field = 'column_id,column_siteid,column_pid,column_path,column_icon,column_moduleid,column_name';
        $list = SiteColumnModel::with('module')->field($field)->where('column_siteid=' . $siteid)->order('column_sort asc')->select()->toArray();

        // 缓存数据
        cache('cache_columns_' . $siteid, $list);

        return $list;
    }

    /**
     * 栏目内容
     */
    protected function getColumn($columnid)
    {
        // 存在缓存，直接返回缓存数据
        if (cache('?cache_column_' . $columnid)) {
            return cache('cache_column_' . $columnid);
        }

        $column = SiteColumnModel::get($columnid);

        // 缓存数据
        cache('cache_column_' . $columnid, $column);

        return $column;
    }

    /**
     * [getSiteContent 站点自定义内容]
     * @param  [type] $site [description]
     * @return [type]       [description]
     */
    protected function getSiteContent($siteid)
    {
        if (cache('?cache_content_' . $siteid)) {
            return cache('cache_content_' . $siteid);
        }

        $where = 'content_siteid = ' . $siteid . ' and content_columnid = 0';
        $content = SiteContentModel::where($where)->select()->toArray();

        //dump($content);

        // 缓存数据
        cache('cache_content_' . $siteid, $content);

        return $content;
    }

    // 验证post提交
    protected function testPost()
    {
        if (!$this->request->isPost()) {
            $this->error('非法操作!(post)');
        }
    }

    // 验证get提交
    protected function testGet()
    {
        if (!$this->request->isGet()) {
            $this->error('非法操作!(get)');
        }
    }

    // 验证put提交
    protected function testPut()
    {
        if (!$this->request->isPut()) {
            $this->error('非法操作!(put)');
        }
    }

    // 验证delete提交
    protected function testDel()
    {
        if (!$this->request->isDelete()) {
            $this->error('非法操作!(delete)');
        }
    }

}
