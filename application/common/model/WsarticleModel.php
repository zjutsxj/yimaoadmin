<?php

namespace app\common\model;

use app\common\model\SiteContentModel;
use Overtrue\Pinyin\Pinyin;
use think\Model;

class WsarticleModel extends Model
{
    protected $pk = 'wsarticle_id'; //主键

    // 模型事件
    protected static function init()
    {
        // 新增前
        self::beforeInsert(function ($value) {
            if (isset($value['wsarticle_author']) && $value['wsarticle_author'] == '') {
                $manage = cookie('admin_manage');
                $value['wsarticle_author'] = $manage['manage_fullname'];
            }

            // 处理内容描述
            if (isset($value['wsarticle_details']) && $value['wsarticle_details'] != '') {

                $desc = strip_tags($value['wsarticle_details']);
                $desc = $desc != '' ? sub_str(trim($desc), 100) : '';

                if (isset($value['wsarticle_desc']) && $value['wsarticle_desc'] == '') {
                    $value['wsarticle_desc'] = $desc;
                }

                if (isset($value['wsarticle_description']) && $value['wsarticle_description'] == '') {
                    $value['wsarticle_description'] = $desc;
                }
            }

            // 分类
            if (isset($value['wsarticle_cateid']) && is_array($value['wsarticle_cateid'])) {
                $value['wsarticle_cateid'] = implode(',', $value['wsarticle_cateid']);
            }

            if (!isset($value['wsarticle_title']) || $value['wsarticle_title'] == '') {
                return $value;
            }

            if (!isset($value['wsarticle_page'])) {
                return $value;
            }

            if ($value['wsarticle_page'] != '') {
                return $value;
            }

            $pinyin = new Pinyin();
            $page = strtolower($pinyin->permalink($value['wsarticle_title']));
            $page = implode('-', array_slice(explode('-', $page), 0, 8));
            $value['wsarticle_page'] = $page;
            return $value;
        });

        // 更新前
        self::beforeUpdate(function ($value) {
            if (isset($value['wsarticle_author']) && $value['wsarticle_author'] == '') {
                $manage = cookie('admin_manage');
                $value['wsarticle_author'] = $manage['manage_fullname'];
            }

            // 处理内容描述
            if (isset($value['wsarticle_details']) && $value['wsarticle_details'] != '') {

                $desc = strip_tags($value['wsarticle_details']);
                $desc = $desc != '' ? sub_str(trim($desc), 100) : '';

                if (isset($value['wsarticle_desc']) && $value['wsarticle_desc'] == '') {
                    $value['wsarticle_desc'] = $desc;
                }

                if (isset($value['wsarticle_description']) && $value['wsarticle_description'] == '') {
                    $value['wsarticle_description'] = $desc;
                }
            }
            
            // 分类
            if (isset($value['wsarticle_cateid']) && is_array($value['wsarticle_cateid'])) {
                $value['wsarticle_cateid'] = implode(',', $value['wsarticle_cateid']);
            }

            if (!isset($value['wsarticle_page'])) {
                return $value;
            }

            if (!isset($value['wsarticle_title']) || $value['wsarticle_title'] == '') {
                return $value;
            }

            if ($value['wsarticle_page'] != '') {
                return $value;
            }

            $pinyin = new Pinyin();
            $page = strtolower($pinyin->permalink($value['wsarticle_title']));
            $page = implode('-', array_slice(explode('-', $page), 0, 8));
            $value['wsarticle_page'] = $page;
            return $value;
        });

        // 删除前 删除相关的图片文件
        self::beforeDelete(function ($value) {
            $images = $value['wsarticle_image'];
            if (is_array($images)) {
                foreach ($images as $key => $image) {
                    del_file($image['file_path']);
                }
            }

            $files = $value['wsarticle_file'];
            if (is_array($files)) {
                foreach ($files as $key => $file) {
                    del_file($file['file_path']);
                }
            }

            return $value;
        });

        // 删除后
        self::afterDelete(function ($value) {
            // 删除自定义字段内容
            /*$where = 'content_articleid = ' . $value['wsarticle_id'];
            SiteContentModel::where($where)->delete();*/
            // 用 destroy 函数删除数据 触发模型事件
            $articleid = $value['wsarticle_id'];
            SiteContentModel::destroy(function ($query) use ($articleid) {
                $query->where('content_articleid', '=', $articleid);
            });
            return $value;
        });
    }

    // 发布时间
    public function setWsarticleCreateTimeAttr($value)
    {
        if ($value == '') {
            $value = time();
        } else {
            $value = strtotime($value);
        }
        return $value;
    }

    // 发布时间
    public function getWsarticleCreateTimeAttr($value)
    {
        $value = date('Y-m-d H:i:s', $value);
        return $value;
    }

    // 更新时间
    public function setWsarticleUpdateTimeAttr($value)
    {
        if ($value == '') {
            $value = time();
        } else {
            $value = strtotime($value);
        }
        return $value;
    }

    // 更新时间
    public function getWsarticleUpdateTimeAttr($value)
    {
        $value = date('Y-m-d H:i:s', $value);
        return $value;
    }

    // 图片进行序列化
    public function setWsarticleImageAttr($value)
    {
        return serialize($value);
    }

    // 序列化
    public function getWsarticleImageAttr($value)
    {
        return unserialize($value);
    }

    // 文件进行序列化
    public function setWsarticleFileAttr($value)
    {
        return serialize($value);
    }

    // 序列化
    public function getWsarticleFileAttr($value)
    {
        return unserialize($value);
    }

    // 获取器 wsarticle_cate_html 新添加字段
    public function getWsarticleHomeHtmlAttr($value, $data)
    {
        $value = $data['wsarticle_home'];
        $status = [
            0 => '',
            1 => '<i class="icon icon-check text-success" title="首页"></i>',
        ];
        return isset($status[$value]) ? $status[$value] : '';
    }

    // 获取器 wsarticle_field_html 新添加字段
    public function getWsarticleRecommendHtmlAttr($value, $data)
    {
        $value = $data['wsarticle_recommend'];
        $status = [
            0 => '',
            1 => '<i class="icon icon-check text-success" title="推荐"></i>',
        ];
        return isset($status[$value]) ? $status[$value] : '';
    }
}
