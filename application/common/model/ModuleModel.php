<?php

namespace app\common\model;

use think\Model;

class ModuleModel extends Model
{
    protected $pk = 'module_id'; //主键
    protected $autoWriteTimestamp = true; //自动时间
    // 定义时间戳字段名
    protected $createTime = 'module_create_time';
    protected $updateTime = 'module_update_time';

    // 获取器 module_cate_html 是否支持分类
    public function getModuleCateHtmlAttr($value, $data)
    {
        $value = $data['module_cate'];
        $status = [
            0 => '&nbsp;',
            1 => '<label class="label label-success">支持</label>',
        ];
        return isset($status[$value]) ? $status[$value] : '';
    }

    // 获取器 module_field_html 是否支持自定义字段
    public function getModuleFieldHtmlAttr($value, $data)
    {
        $value = $data['module_field'];
        $status = [
            0 => '&nbsp;',
            1 => '<label class="label label-success">支持</label>',
        ];
        return isset($status[$value]) ? $status[$value] : '';
    }

    // 模型事件
    protected static function init()
    {
        // 更新站点后
        self::afterUpdate(function ($module) {

        });

        // 删除站点后 删除相关的其它文件
        self::beforeDelete(function ($module) {

        });
    }
}
