<?php

namespace app\common\model;

use think\Model;

class SiteParamModel extends Model
{
    protected $pk = 'param_id'; //主键
    protected $autoWriteTimestamp = true; //自动时间
    // 定义时间戳字段名
    protected $createTime = 'param_create_time';
    protected $updateTime = 'param_update_time';

    // 模型事件
    protected static function init()
    {

        // 更新栏目后
        self::afterUpdate(function ($value) {

        });

        // 删除栏目前
        self::beforeDelete(function ($value) {

        });

        // 删除字段后
        self::afterDelete(function ($value) {
            $paramid = $value['param_id'];
            SiteContentModel::where('content_paramid=' . $paramid)->delete();
        });
    }

    // 获取器 param_cate_html 新添加字段
    public function getParamTypeHtmlAttr($value, $data)
    {
        $value = $data['param_type'];
        $status = [
            'text' => '单行文本框',
            'number' => '数字文本框',
            'email' => '邮箱文本框',
            'textarea' => '多行文本框',
            'select' => '下拉框',
            'checkbox' => '多选框',
            'radio' => '单选框',
            'file' => '附件上传',
            'editor' => '编辑器',
            'image' => '图片上传',
        ];
        return isset($status[$value]) ? $status[$value] : '';
    }

    // 获取器 param_field_html 新添加字段
    public function getParamRequiredHtmlAttr($value, $data)
    {
        $value = $data['param_required'];
        $status = [
            0 => '<label class="label label-danger">否</label>',
            1 => '<label class="label label-success">是</label>',
        ];
        return isset($status[$value]) ? $status[$value] : '';
    }

    // 获取站点自定义字段
    public static function getSiteParam($siteid)
    {
        $where = 'param_siteid = ' . $siteid . ' and param_columnid = 0';
        return self::where($where)->order('param_sort asc,param_id asc')->select()->toArray();
    }

    // 获取栏目自定义字段
    public static function getColumnParam($siteid, $columnid)
    {
        $where = 'param_siteid = ' . $siteid . ' and param_columnid = ' . $columnid;
        return self::where($where)->order('param_sort asc,param_id asc')->select()->toArray();
    }
}
