<?php

namespace app\common\model;

use think\Model;

class WsmessageModel extends Model
{
    protected $pk = 'wsmessage_id'; //主键
    protected $autoWriteTimestamp = true; //自动时间
    // 定义时间戳字段名
    protected $createTime = 'wsmessage_create_time';
    protected $updateTime = 'wsmessage_update_time';

    public static function init()
    {
        self::beforeInsert(function ($value) {
            if (isset($value['wsmessage_ip'])) {
                $value['wsmessage_ip'] = request()->ip();
            }
            return $value;
        });
    }

    // 获取器 status_html 新添加字段
    public function getWsmessageStatusHtmlAttr($value, $data)
    {
        $value = $data['wsmessage_status'];
        $status = [
            0 => '<label class="label label-warning">审核</label>',
            1 => '<label class="label label-success">完成</label>',
        ];
        return isset($status[$value]) ? $status[$value] : '';
    }

}
