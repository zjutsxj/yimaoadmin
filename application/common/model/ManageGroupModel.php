<?php

namespace app\common\model;

use think\Model;

class ManageGroupModel extends Model
{
    protected $pk = 'group_id'; //主键
    protected $autoWriteTimestamp = true; //自动时间
    // 定义时间戳字段名
    protected $createTime = 'group_create_time';
    protected $updateTime = 'group_update_time';

    // 模型事件
    protected static function init()
    {
        // 更新后
        self::afterUpdate(function ($value) {
            // 删除缓存
            $cache_name = 'cache_manage_group_' . $value['group_id'];
            if (cache('?' . $cache_name)) {
                cache($cache_name, null);
            }
        });
    }
}
