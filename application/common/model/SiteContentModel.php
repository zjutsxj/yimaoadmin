<?php

namespace app\common\model;

use app\common\model\SiteParamModel;
use think\Model;

class SiteContentModel extends Model
{
    protected $pk = 'content_id'; //主键

    // 模型事件
    protected static function init()
    {

        // 更新栏目后
        self::afterUpdate(function ($value) {

        });

        // 删除内容前 删除相关的其它文件
        self::beforeDelete(function ($value) {
            // 删除文件
            if ($value['content_type'] == 'image' && $value['content_value'] != '') {
                del_file($value['content_value']);
            }

            if ($value['content_type'] == 'file' && $value['content_value'] != '') {
                del_file($value['content_value']);
            }

            return $value;
        });
    }

    // 网站设置 自定义 字段内容
    public static function getSiteContent($siteid)
    {
        $where = 'content_siteid = ' . $siteid . ' and content_columnid = 0';
        $list = self::where($where)->column('content_paramid, content_type, content_value', 'content_paramid');
        $content = array();
        foreach ($list as $paramid => $value) {
            if ($value['content_type'] == 'checkbox') {
                $content['param_' . $paramid] = unserialize($value['content_value']);
            } else {
                $content['param_' . $paramid] = $value['content_value'];
            }
        }
        return $content;
    }

    // 保存网站自定义字段
    public static function setSiteContent($siteid, $request)
    {
        $param = SiteParamModel::getSiteParam($siteid);
        // 处理前台提交的数据
        $data = array();
        foreach ($param as $item) {
            // 判断字段是否存在
            $field = 'param_' . $item['param_id'];
            if (!isset($request[$field])) {
                continue;
            }
            // 判断字段是否有值
            $value = $request[$field];
            
            // 序列化 checkbox 的值，并且在字符串前加特殊字符
            $value = $item['param_type'] == 'checkbox' ? serialize($value) : $value;
            $data[] = [
                'content_siteid' => $item['param_siteid'],
                'content_paramid' => $item['param_id'],
                'content_type' => $item['param_type'],
                'content_name' => $item['param_name'],
                'content_value' => $value,
            ];
        }

        if ($data) {
            // 删除数据
            self::where('content_siteid=' . $siteid . ' and content_columnid=0')->delete();
            // 保存数据
            $model = new SiteContentModel;
            $model->saveAll($data);
        }

        // 清空缓存
        if (cache('?cache_content_' . $siteid)) {
            cache('cache_content_' . $siteid, null);
        }

        return true;
    }

    // 网站栏目 自定义 字段内容
    public static function getColumnContent($siteid, $columnid, $articleid)
    {
        $where = 'content_siteid = ' . $siteid . ' and content_columnid = ' . $columnid . ' and content_articleid = ' . $articleid;
        $list = self::where($where)->column('content_paramid, content_type, content_value', 'content_paramid');
        $content = array();
        foreach ($list as $paramid => $value) {
            if ($value['content_type'] == 'checkbox') {
                $content['param_' . $paramid] = unserialize($value['content_value']);
            } else {
                $content['param_' . $paramid] = $value['content_value'];
            }
        }
        return $content;
    }

    // 保存网站栏目自定义字段
    public static function setColumnContent($siteid, $columnid, $articleid, $request)
    {
        $param = SiteParamModel::getColumnParam($siteid, $columnid);

        // 处理前台提交的数据
        $data = array();
        foreach ($param as $item) {
            // 判断字段是否存在
            $field = 'param_' . $item['param_id'];
            if (!isset($request[$field])) {
                continue;
            }
            // 判断字段是否有值
            $value = $request[$field];
            if ($value == '') {
                continue;
            }
            // 序列化 checkbox 的值，并且在字符串前加特殊字符
            $value = $item['param_type'] == 'checkbox' ? serialize($value) : $value;
            $data[] = [
                'content_siteid' => $item['param_siteid'],
                'content_columnid' => $item['param_columnid'],
                'content_paramid' => $item['param_id'],
                'content_type' => $item['param_type'],
                'content_name' => $item['param_name'],
                'content_articleid' => $articleid,
                'content_value' => $value,
            ];
        }
        if ($data) {
            // 删除数据
            $where = 'content_siteid = ' . $siteid . ' and content_columnid = ' . $columnid . ' and content_articleid = ' . $articleid;
            self::where($where)->delete();

            // 保存数据
            $model = new SiteContentModel;
            $model->saveAll($data);
        }

        return true;
    }

    // 单页 自定义 字段内容
    public static function getPageContent($siteid, $columnid)
    {
        $where = 'content_siteid = ' . $siteid . ' and content_columnid = ' . $columnid;
        $list = self::where($where)->column('content_paramid, content_type, content_value', 'content_paramid');
        $content = array();
        foreach ($list as $paramid => $value) {
            if ($value['content_type'] == 'checkbox') {
                $content['param_' . $paramid] = unserialize($value['content_value']);
            } else {
                $content['param_' . $paramid] = $value['content_value'];
            }
        }
        return $content;
    }

    // 保存单页自定义字段
    public static function setPageContent($siteid, $columnid, $request)
    {
        $param = SiteParamModel::getColumnParam($siteid, $columnid);

        // 处理前台提交的数据
        $data = array();
        foreach ($param as $item) {
            // 判断字段是否存在
            $field = 'param_' . $item['param_id'];
            if (!isset($request[$field])) {
                continue;
            }
            // 判断字段是否有值
            $value = $request[$field];
            if ($value == '') {
                continue;
            }
            // 序列化 checkbox 的值，并且在字符串前加特殊字符
            $value = $item['param_type'] == 'checkbox' ? serialize($value) : $value;
            $data[] = [
                'content_siteid' => $item['param_siteid'],
                'content_columnid' => $item['param_columnid'],
                'content_paramid' => $item['param_id'],
                'content_type' => $item['param_type'],
                'content_name' => $item['param_name'],
                'content_articleid' => 0,
                'content_value' => $value,
            ];
        }
        if ($data) {
            // 删除数据
            $where = 'content_siteid = ' . $siteid . ' and content_columnid = ' . $columnid;
            self::where($where)->delete();

            // 保存数据
            $model = new SiteContentModel;
            $model->saveAll($data);
        }

        return true;
    }
}
