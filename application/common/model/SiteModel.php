<?php

namespace app\common\model;

use app\common\model\SiteColumnModel;
use app\common\model\SiteParamModel;
use think\Model;

class SiteModel extends Model
{
    protected $pk = 'site_id'; //主键
    protected $autoWriteTimestamp = true; //自动时间
    // 定义时间戳字段名
    protected $createTime = 'site_create_time';
    protected $updateTime = 'site_update_time';

    // 模型事件
    protected static function init()
    {
        // 新建站点后
        self::afterInsert(function ($value) {
            $sites = SiteModel::order('site_default asc')->column('site_mark, site_name,site_default, site_domain', 'site_id');

            // 生成路由
            make_index_route($sites);

            // 生成控制器
            make_index_controller($sites);

            // 清空缓存
            if (cache('?cache_site_list')) {
                cache('cache_site_list', null);
            }
        });

        // 更新站点后
        self::afterUpdate(function ($value) {
            $sites = SiteModel::order('site_default asc')->column('site_mark,site_name,site_domain,site_default', 'site_id');

            // 生成路由
            make_index_route($sites);

            // 生成控制器
            make_index_controller($sites);

            // 清空缓存
            if (cache('?cache_site_list')) {
                cache('cache_site_list', null);
            }

            if (cache('?cache_site_' . $value['site_id'])) {
                cache('cache_site_' . $value['site_id'], null);
            }
        });

        // 删除站点后
        self::afterDelete(function ($value) {

            // 删除栏目
            SiteColumnModel::where('column_siteid=' . $value['site_id'])->delete();

            // 删除自定义字段
            SiteParamModel::where('param_siteid=' . $value['site_id'])->delete();

            $sites = SiteModel::order('site_default asc')
                ->column('site_mark, site_name, site_default, site_domain', 'site_id');

            // 生成路由
            make_index_route($sites);

            // 生成控制器
            make_index_controller($sites);

            // 清空缓存
            if (cache('?cache_site_list')) {
                cache('cache_site_list', null);
            }

            if (cache('?cache_site_' . $value['site_id'])) {
                cache('cache_site_' . $value['site_id'], null);
            }
        });
    }

    // 关联模型 栏目
    public function column()
    {
        return $this->hasMany('SiteColumnModel', 'column_siteid', 'site_id');
    }

    // 自定义字段
    public function param()
    {
        return $this->hasMany('SiteParamModel', 'param_siteid', 'site_id');
    }

    public function content()
    {
        return $this->hasMany('SiteContentModel', 'content_siteid', 'site_id');
    }
}
