<?php
namespace app\common\behavior;

/**
 * Smsbao 短信宝
 */
class SmsbaoBehavior
{
    public function run($params)
    {
        $phone = $params['phone'];
        $text = $params['text'];

        $smsapi = "http://api.smsbao.com/";
        $user = "******"; //短信平台帐号
        $pass = md5("******"); //短信平台密码
        $sign = "【Yimao软件】";

        $content = $sign . $text;

        $sendurl = $smsapi . "sms?u=" . $user . "&p=" . $pass . "&m=" . $phone . "&c=" . urlencode($content);
        $result = file_get_contents($sendurl);
        if ($result == '0') {
            return true;
        }

        $statusStr = array(
            "0" => "短信发送成功",
            "-1" => "参数不全",
            "-2" => "服务器空间不支持,请确认支持curl或者fsocket！",
            "30" => "密码错误",
            "40" => "账号不存在",
            "41" => "余额不足",
            "42" => "帐户已过期",
            "43" => "IP地址限制",
            "50" => "内容含有敏感词",
        );
        return $statusStr[$result];
    }
}
