Route::rule('{site_mark}/:action/<catename>-<cateid>-p<page>', 'index/{uc_site_mark}/:action');
Route::rule('{site_mark}/:action/<catename>-<cateid>', 'index/{uc_site_mark}/:action');
Route::rule('{site_mark}/:action-p<page>', 'index/{uc_site_mark}/:action');
Route::rule('{site_mark}/:action/<id>', 'index/{uc_site_mark}/:action');
Route::rule('{site_mark}/:action', 'index/{uc_site_mark}/:action');
Route::rule('{site_mark}/', 'index/{uc_site_mark}/index');