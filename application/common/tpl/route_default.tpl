Route::rule(':action/<catename>-<cateid>-p<page>', 'index/{uc_site_mark}/:action');
Route::rule(':action/<catename>-<cateid>', 'index/{uc_site_mark}/:action');
Route::rule(':action/<id>', 'index/{uc_site_mark}/:action');
Route::rule(':action-p<page>', 'index/{uc_site_mark}/:action');
Route::rule(':action', 'index/{uc_site_mark}/:action');
Route::rule('', 'index/{uc_site_mark}/index');