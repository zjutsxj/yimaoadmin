{__NOLAYOUT__}<!DOCTYPE html>
<html lang="zh_CN">

<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>跳转提示</title>
    <link rel="stylesheet" type="text/css" href="__STATIC__/css/zui.min.css">
    <style type="text/css">
        body{font-family: 'Microsoft YaHei'; font-size: 14px;background-color: #039BE5; background-image: url('__STATIC__/images/login_bg.png');background-size: cover;}
        .modal-dialog {border-radius: 0; border-width: 0;}
        .modal-backdrop.in {opacity: 0;}
        .modal-header {padding: 10px;}
        .modal-body{padding: 0;}
        .modal-footer{padding: 10px;}
        .alert{margin-bottom: 0px;}
        .content{font-size: 16px;}
        @media (min-width: 768px) {.modal-dialog {width: 500px;}}
    </style>
</head>
<body>
<div class="modal" data-backdrop="static" data-keyboard="false" id="message">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">提示信息</h4>
            </div>
            <div class="modal-body">
                <?php if($code == 1) { ?>
                    <div class="alert alert-success with-icon">
                        <i class="icon-ok-sign"></i>
                        <div class="content"><?php echo(strip_tags($msg));?></div>
                    </div>
                <?php } ?>
                <?php if($code == 0) { ?>
                    <div class="alert alert-danger with-icon">
                        <i class="icon-frown"></i>
                        <div class="content"><?php echo(strip_tags($msg));?></div>
                    </div>
                <?php } ?>
            </div>
            <div class="modal-footer text-right">
                页面自动 <a id="href" href="<?php echo($url);?>">跳转</a> 等待时间： <b id="wait" class="text-danger"><?php echo($wait);?></b>&nbsp;&nbsp;秒
            </div>
        </div>
    </div>
</div>
<script src="__STATIC__/lib/jquery/jquery.js"></script>
<script src="__STATIC__/js/zui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#message").modal('show');
        var wait = $('#wait'), href = $('#href').attr('href');
        var interval = setInterval(function(){
            var time = parseInt(wait.html()), time = time - 1;
            wait.html(time);
            if(time <= 0) {
                location.href = href;
                clearInterval(interval);
            };
        }, 1000);
    });
</script>
</body>
</html>
