<?php
namespace app\index\controller;

use app\common\controller\HomeBaseController;
use think\facade\Env;

/**
 * 默认控制器
 * www.abc.com/aboutus.html
 */
class EmptyController extends HomeBaseController
{
    public function index()
    {
        // 项目根目录
        $tplpath = Env::get('root_path');
        $tplpath .= 'public/';

        // 模板路径
        $tplpath .= config('template.view_path');
        $tplpath .= '{site_mark}/';
        $tplpath .= $this->request->controller();
        $tplpath .= '.html';

        $tplpath = strtolower($tplpath);

        if (file_exists($tplpath)) {
            return $this->fetch($tplpath);
        }

        self::page_404('Empty/index <br> 模板文件不存在 <br>' . $tplpath);
    }

    /**
     * 处理
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function _empty()
    {
        // 项目根目录
        $tplpath = Env::get('root_path');
        $tplpath .= 'public/';

        // 模板路径
        $tplpath .= config('template.view_path');
        $tplpath .= '{site_mark}/';
        $tplpath .= $this->request->controller();
        $tplpath .= $this->request->action();
        $tplpath .= '.html';

        $tplpath = strtolower($tplpath);

        if (file_exists($tplpath)) {
            return $this->fetch($tplpath);
        }

        self::page_404('Empty/_empty <br> 模板文件不存在 <br>' . $tplpath);
    }
}
