<?php
namespace app\api\controller;

use app\common\controller\ApiBaseController;
use GuzzleHttp\Client;

class IndexController extends ApiBaseController
{
    public function index(){
        $url = 'http://119.23.233.52/api/api/user/GetLoginToken';
        $client = new Client();
        $response = $client->post($url,[
            'headers' => [
                'Content-Type' => 'application/json'
            ],//设置请求头为json
            'json'=>[
                'LoginName'=>'HYYY1',
                'PassWord'=>'123456',
            ]
        ]);
        $cont = $response->getBody()->getContents();
        $json = json_decode($cont);
        dump($json);
        //dump($response->getStatusCode());
    }

    public function getinfo(){
        $url = 'http://119.23.233.52/api/api/vehicle/GetCurrentVehicleInfo';
        $client = new Client();
        $response = $client->post($url, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer 775E5861EA643993FB666BCE293EE8CC'
            ],//设置请求头为json
            'json'=>[
                'VehiclePlates'=>'2'
            ]
        ]);
        $cont = $response->getBody()->getContents();
        $json = json_decode($cont);
        dump($json);
        //dump($response->getStatusCode());
    }

    public function test()
    {
        $data = [
            ['name' => 'zjutsxj', 'age' => 27, 'phone' => '13588888888', 'email' => 'zjutsxj@qq.com'],
            ['name' => 'zjutsxj', 'age' => 27, 'phone' => '13588888888', 'email' => 'zjutsxj@qq.com'],
            ['name' => 'zjutsxj', 'age' => 27, 'phone' => '13588888888', 'email' => 'zjutsxj@qq.com'],
            ['name' => 'zjutsxj', 'age' => 27, 'phone' => '13588888888', 'email' => 'zjutsxj@qq.com'],
            ['name' => 'zjutsxj', 'age' => 27, 'phone' => '13588888888', 'email' => 'zjutsxj@qq.com'],
            ['name' => 'zjutsxj', 'age' => 27, 'phone' => '13588888888', 'email' => 'zjutsxj@qq.com'],
        ];
        echo json_encode($data);
    }

    public function abc()
    {
        dump(request()->routeInfo());
    }

    public function _empty()
    {
        dump(request()->routeInfo());
    }
}
