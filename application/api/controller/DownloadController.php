<?php
namespace app\api\controller;

use app\common\controller\ApiBaseController;
use app\common\model\WsarticleModel;

class DownloadController extends ApiBaseController
{
    public function index($id){
        // 更新下载
        $model = WsarticleModel::get($id);
        if(!$model){
            return '文件不存在';
        }

        $files = $model->wsarticle_file;
        if ($files == '' && $files != null && $files != 'a:0:{}') {
            return '文件不存在';
        }

        if (!is_array($files)) {
            return '文件不存在';
        }

        if (count($files) <= 0) {
            return '文件不存在';
        }

        $files = array_values($files);
        if (!isset($files[0]['file_path'])) {
            return '文件不存在';
        }

        $file_path = $files[0]['file_path'];
        //dump($file_path);
        $ext = substr(strrchr($file_path, '.'), 1);

        // 更新下载量
        $model->wsarticle_views = $model->wsarticle_views + 1;
        $model->save();

        // 文件下载
        return download('.' . $file_path, $id . '.' . $ext);
    }
}