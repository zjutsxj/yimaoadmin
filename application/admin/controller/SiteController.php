<?php
namespace app\admin\controller;

use app\admin\validate\SiteColumnValidate;
use app\admin\validate\SiteParamValidate;
use app\admin\validate\SiteValidate;
use app\common\controller\AdminBaseController;
use app\common\model\ModuleModel;
use app\common\model\SiteColumnModel;
use app\common\model\SiteModel;
use app\common\model\SiteParamModel;
use think\facade\Config;
use think\Request;

/**
 *
 */
class SiteController extends AdminBaseController
{
    /**
     * 站点列表
     */
    public function index()
    {
        $keywords = $this->request->param('keywords');
        $this->assign('keywords', $keywords);

        $where = [];
        $where[] = ['site_default', '>=', '0'];
        if ($keywords != '') {
            $where[] = ['site_name|site_mark|site_desc', 'like', '%' . $keywords . '%'];
        }
        $list = SiteModel::where($where)->order('site_sort asc,site_id asc')->select();
        $this->assign('list', $list);

        return $this->fetch();
    }

    /**
     * 设置默认首页
     */
    public function home($id)
    {
        SiteModel::where('site_id <>' . $id)->update(['site_default' => 0]);

        // 触发模型事件
        SiteModel::update(['site_id' => $id, 'site_default' => '1']);

        // 生成 route/index.php 路由文件
        // 生成 index/controller/控制器文件
        $this->success('恭喜：设置成功!', null, '', 1);
    }

    /**
     * 保存新增
     */
    public function save()
    {
        self::testPost();

        // 获取数据
        $data = $this->request->param();
        unset($data['site_id']);

        // 验证数据
        $validate = new SiteValidate();
        if (!$validate->sceneCreate()->check($data)) {
            $this->error($validate->getError());
        }

        // 保存数据
        $field = ['site_name', 'site_domain', 'site_mark', 'site_default', 'site_desc'];
        $site1 = SiteModel::create($data, $field);
        if (!$site1) {
            $this->error('错误：新增失败!');
        }
        $this->success('恭喜：新增成功!', null, '', 1);
    }

    /**
     * 修改
     */
    public function edit($id)
    {
        $field = 'site_id,site_name,site_mark,site_domain,site_default,site_desc';
        $site = SiteModel::field($field)->find($id);
        $this->success('恭喜：查询成功', null, $site, 1);
    }

    /**
     * 保存修改
     */
    public function update()
    {
        self::testPost();

        $data = $this->request->param();
        // 验证数据
        $validate = new SiteValidate();
        if (!$validate->sceneUpdate()->check($data)) {
            $this->error($validate->getError());
        }

        $site = SiteModel::update($data);

        if (!$site) {
            $this->error('错误：修改失败!');
        }

        $this->success('恭喜：修改成功!', null, '', 1);
    }

    /**
     * 修改站点排序
     */
    public function sort()
    {
        self::testPost();

        $id = $this->request->param('id', 0);
        $sort = $this->request->param('sort', 0);

        $data['site_id'] = $id;
        $data['site_sort'] = $sort;

        SiteModel::update($data);

        $this->success('恭喜：排序成功!', null, ['jump' => 'no'], 1);
    }

    /**
     * 删除站点
     */
    public function delete($id)
    {
        self::testDel();
        // $id 数据格式
        // $id：1
        // $id：1，2，3
        $site = SiteModel::destroy($id);

        if (!$site) {
            $this->error('错误：删除失败!');
        }

        $this->success('恭喜：删除成功!', null, '', 1);
    }

    /**
     * 栏目列表
     */
    public function column($siteid)
    {
        $site = SiteModel::get($siteid);
        $this->assign('site', $site);

        $keywords = $this->request->param('keywords');
        $this->assign('keywords', $keywords);

        $where = [];
        $where[] = ['column_siteid', '=', $siteid];
        if ($keywords != '') {
            $where[] = ['column_name|column_title', 'like', '%' . $keywords . '%'];
        }
        $list = SiteColumnModel::with('module')->where($where)->order('column_sort asc')->select();
        $this->assign('list', $list);

        $module = ModuleModel::order('module_sort asc, module_id asc')->select();
        $this->assign('module', $module);

        return $this->fetch('site/column/index');
    }

    /**
     * 保存新增栏目
     */
    public function column_save()
    {
        self::testPost();
        // 获取数据
        $data = $this->request->param();
        unset($data['column_id']);

        // 验证数据
        $validate = new SiteColumnValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 保存数据
        $field = ['column_siteid', 'column_pid', 'column_path', 'column_moduleid', 'column_icon', 'column_name', 'column_enname', 'column_cate', 'column_field', 'column_title', 'column_sort'];
        $column = SiteColumnModel::create($data, $field);
        if (!$column) {
            $this->error('错误：新增失败!');
        }
        $this->success('恭喜：新增成功!', null, '', 1);
    }

    /**
     * 修改栏目
     */
    public function column_edit($id)
    {
        $field = 'column_id, column_pid, column_path, column_moduleid, column_icon, column_name, column_enname, column_cate, column_field';
        $column = SiteColumnModel::field($field)->find($id);
        $this->success('恭喜：查询成功', null, $column, 1);
    }

    /**
     * 保存修改栏目
     */
    public function column_update()
    {
        self::testPost();

        $data = $this->request->param();
        // 验证数据
        $validate = new SiteColumnValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        $column = SiteColumnModel::update($data);

        if (!$column) {
            $this->error('错误：修改失败!');
        }

        $this->success('恭喜：修改成功!', null, '', 1);
    }

    /**
     * 删除栏目
     */
    public function column_delete($siteid, $id)
    {
        self::testDel();
        // $id 数据格式
        // $id：1
        // $id：1，2，3
        $model = SiteColumnModel::destroy($id);

        if (!$model) {
            $this->error('错误：删除失败!');
        }

        $model = new SiteColumnModel;
        $model->update_sort($siteid);

        $this->success('恭喜：删除成功!', null, '', 1);
    }

    /**
     * 栏目排序列表
     */
    public function column_sort($siteid)
    {
        $site = SiteModel::get($siteid);
        $this->assign('site', $site);

        $site = new SiteColumnModel();
        $where = array();
        $where[] = ['column_siteid', '=', $siteid];
        $list = $site->where($where)->order('column_sort asc,column_id asc')->select();
        // 转化为数组
        $list = $list->toArray();
        // 列表转化为树结构
        $tree = list_to_tree($list, 'column_id', 'column_pid');
        // 生成html
        $html = $site->tree_to_html($tree);
        unset($list, $tree);

        $this->assign('html', $html);
        $this->assign('backurl', $_SERVER['HTTP_REFERER']);

        return $this->fetch('site/column/sort');
    }

    /**
     * 保存栏目排序
     */
    public function column_sort_save()
    {
        self::testPost();
        $json = $this->request->param('column_json');
        $tree = json_decode($json, true);

        if (count($tree) == 0) {
            $this->error("栏目没有排序！");
        }

        $column = new SiteColumnModel();
        $data = $column->tree_to_data($tree);
        $column->saveAll($data);
        $this->success('恭喜：排序成功!', $this->request->param('backurl'), '', 1);
    }

    /**
     * 参数列表
     */
    public function param($siteid, $columnid = 0)
    {
        $site = SiteModel::get($siteid);
        $this->assign('site', $site);

        $column = SiteColumnModel::get($columnid);
        $this->assign('column', $column);

        $param_type = Config::get('param_type');
        $this->assign('param_type', $param_type);

        $keywords = $this->request->param('keywords');
        $this->assign('keywords', $keywords);

        $where = array();
        $where[] = ['param_siteid', '=', $siteid];
        $where[] = ['param_columnid', '=', $columnid];
        if ($keywords != '') {
            $where[] = ['param_name', 'like', '%' . $keywords . '%'];
        }
        $list = SiteParamModel::where($where)->order('param_sort asc,param_id asc')->select();
        $this->assign('list', $list);

        return $this->fetch('site/param/index');
    }

    /**
     * 保存参数
     */
    public function param_save()
    {
        self::testPost();
        // 获取数据
        $data = $this->request->param();
        unset($data['column_id']);

        // 验证数据
        $validate = new SiteParamValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 保存数据
        $field = ['param_siteid', 'param_columnid', 'param_name', 'param_type', 'param_tips', 'param_value', 'param_required'];
        $column = SiteParamModel::create($data, $field);
        if (!$column) {
            $this->error('错误：新增失败!');
        }
        $this->success('恭喜：新增成功!', null, '', 1);
    }

    /**
     * 修改参数排序
     */
    public function param_sort()
    {
        self::testPost();

        $id = $this->request->param('id', 0);
        $sort = $this->request->param('sort', 0);

        $data['param_id'] = $id;
        $data['param_sort'] = $sort;

        SiteParamModel::update($data);

        $this->success('恭喜：排序成功!', null, ['jump' => 'no'], 1);
    }

    /**
     * 修改
     */
    public function param_edit($id)
    {
        $field = 'param_id, param_siteid, param_columnid, param_name, param_type, param_tips, param_required, param_value';
        $param = SiteParamModel::field($field)->find($id);
        $this->success('恭喜：查询成功', null, $param, 1);
    }

    /**
     * 保存修改栏目
     */
    public function param_update()
    {
        self::testPost();

        $data = $this->request->param();
        // 验证数据
        $validate = new SiteParamValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        $param = SiteParamModel::update($data);

        if (!$param) {
            $this->error('错误：修改失败!');

        }

        $this->success('恭喜：修改成功!', null, '', 1);
    }

    /**
     * 删除栏目
     */
    public function param_delete($id)
    {
        self::testDel();
        // $id 数据格式
        // $id：1
        // $id：1，2，3
        $param = SiteParamModel::destroy($id);

        if (!$param) {
            $this->error('错误：删除失败!');
        }

        $this->success('恭喜：删除成功!', null, '', 1);
    }
}
