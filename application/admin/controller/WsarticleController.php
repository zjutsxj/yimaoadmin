<?php

namespace app\admin\controller;

use app\admin\validate\WsarticleValidate;
use app\common\controller\AdminBaseController;
use app\common\model\SiteColumnModel;
use app\common\model\SiteContentModel;
use app\common\model\SiteParamModel;
use app\common\model\WsarticleModel;
use app\common\model\WscateModel;
use think\Db;
use think\Request;

class WsarticleController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($siteid, $columnid)
    {
        // 栏目
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);
        // 分类
        $wscate = WscateModel::getList($siteid, $columnid);
        $this->assign('wscate', $wscate);
        // 搜索表单
        $keywords = $this->request->param('keywords');
        $cateid = $this->request->param('cateid', 0);
        $this->assign('keywords', $keywords);
        $this->assign('cateid', $cateid);

        $where = array();
        $where[] = ['wsarticle_siteid', '=', $siteid];
        $where[] = ['wsarticle_columnid', '=', $columnid];
        if ($keywords != '') {
            $where[] = ['wsarticle_title', 'like', '%' . $keywords . '%'];
        }
        if ($cateid > 0) {
            $where[] = ['', 'EXP', Db::raw("FIND_IN_SET('" . $cateid . "', wsarticle_cateid)")];
        }
        $field = 'wsarticle_id, wsarticle_title, wsarticle_image, wsarticle_cateid,wsarticle_home,wsarticle_recommend,wsarticle_create_time,wsarticle_views';
        $order = 'wsarticle_sort desc, wsarticle_id desc';
        $list = WsarticleModel::where($where)->field($field)->order($order)
            ->paginate(10, false, ['query' => $this->request->param()]);
        $this->assign('list', $list);

        return $this->fetch();
    }

    /**
     * 设置分类
     */
    public function cate(Request $request)
    {
        self::testPost();

        $id = $request->param('id');
        $cateid = $request->param('cateid');
        WsarticleModel::where('wsarticle_id in (' . $id . ')')->update(['wsarticle_cateid' => $cateid]);
        $this->success('恭喜：设置成功!', null, '', 1);
    }

    /**
     * 设置属性
     */
    public function attr(Request $request)
    {
        self::testPost();

        $id = $request->param('id');
        $attr = $request->param('attr');
        $value = $request->param('value');
        WsarticleModel::where('wsarticle_id in (' . $id . ')')->update(['wsarticle_' . $attr => $value]);
        $this->success('恭喜：设置成功!', null, '', 1);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create($siteid, $columnid)
    {
        // 栏目
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);

        // 启用分类
        if ($column['column_cate'] > 0) {
            $wscate = WscateModel::getList($siteid, $columnid);
            $this->assign('wscate', $wscate);
        }

        // 启用自定义字段
        if ($column['column_field'] == 1) {
            $param = SiteParamModel::getColumnParam($siteid, $columnid);
            $this->assign('param', $param);
        }

        return $this->fetch();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        self::testPost();
        // 获取数据
        $data = $request->param();
        $backurl = $data['backurl'];

        // 验证数据
        $validate = new WsarticleValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 保存数据
        $model = WsarticleModel::create($data);
        if (!$model) {
            $this->error('提示：新增失败!');
        }

        // 保存自定义字段
        $siteid = $request->param('wsarticle_siteid');
        $columnid = $request->param('wsarticle_columnid');
        $articleid = $model->wsarticle_id;
        SiteContentModel::setColumnContent($siteid, $columnid, $articleid, $data);

        $this->success('恭喜：新增成功!', $backurl, '', 1);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($siteid, $columnid, $id)
    {
        // 栏目
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);

        $wsarticle = WsarticleModel::find($id);
        $this->assign('wsarticle', $wsarticle);

        $cateid = $wsarticle['wsarticle_cateid'];
        $arg = explode(',', $cateid);
        $this->assign('cateid', $arg[0]);
        $this->assign('expand', $arg);

        // 启用分类
        if ($column['column_cate'] > 0) {
            $wscate = WscateModel::getList($siteid, $columnid);
            $this->assign('wscate', $wscate);
        }

        // 启用自定义字段
        if ($column['column_field'] == 1) {
            $param = SiteParamModel::getColumnParam($siteid, $columnid);
            $this->assign('param', $param);
            // 字段内容
            $content = SiteContentModel::getColumnContent($siteid, $columnid, $id);
            $this->assign('content', $content);
        }

        return $this->fetch();
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        self::testPost();

        // 获取数据
        $data = $request->param();
        if (!isset($data['wsarticle_image'])) {
            // 图片不存在
            $data['wsarticle_image'] = [];
        }
        $backurl = $data['backurl'];

        // 验证数据
        $validate = new WsarticleValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 保存数据
        $model = WsarticleModel::update($data);
        if (!$model) {
            $this->error('提示：修改失败!');
        }

        // 保存自定义字段
        $siteid = $request->param('wsarticle_siteid');
        $columnid = $request->param('wsarticle_columnid');
        $articleid = $request->param('wsarticle_id');
        SiteContentModel::setColumnContent($siteid, $columnid, $articleid, $data);

        $this->success('恭喜：修改成功!', $backurl, '', 1);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        self::testDel();

        $model = WsarticleModel::destroy($id);
        if (!$model) {
            $this->error('错误：删除失败!');
        }
        $this->success('恭喜：删除成功!', null, '', 1);
    }

    /**
     * 修改排序
     */
    public function sort(Request $request)
    {
        self::testPost();

        $id = $request->param('id', 0);
        $sort = $request->param('sort', 0);

        // 通过 update 方法更新数据
        WsarticleModel::where('wsarticle_id', $id)->update(['wsarticle_sort' => $sort]);

        $this->success('恭喜：排序成功!', null, ['jump' => 'no'], 1);
    }

    /**
     * 复制内容
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function copy($id)
    {
        self::testGet();

        // 获取原始数据
        $data = WsarticleModel::get($id)->toArray();

        // 生成数组
        $data['wsarticle_image'] = copy_files($data['wsarticle_image']);
        $data['wsarticle_file'] = copy_files($data['wsarticle_file']);
        //
        unset($data['wsarticle_id']);
        // 保存数据
        $model = WsarticleModel::create($data);
        if (!$model) {
            $this->error('提示：复制失败!');
        }

        // 复制自定义字段
        $newid = $model->wsarticle_id;
        $model = new SiteContentModel;
        $list = $model->where('content_articleid=' . $id)->select()->toArray();
        if ($list) {
            $data = array();
            foreach ($list as $key => $value) {
                unset($value['content_id']);
                $item = $value;
                $item['content_articleid'] = $newid;
                if ($value['content_type'] == 'image' || $value['content_type'] == 'file') {
                    $news_path = copy_file($value['content_value']);
                    if (!$news_path) {
                        continue;
                    }
                    $item['content_value'] = $news_path;
                }
                $data[] = $item;
            }
            if ($data) {
                $model->saveAll($data);
            }
        }

        $this->success('恭喜：复制成功!', null, '', 1);
    }
}
