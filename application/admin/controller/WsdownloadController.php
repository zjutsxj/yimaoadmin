<?php

namespace app\admin\controller;

use app\admin\validate\WsdownloadValidate;
use app\common\controller\AdminBaseController;
use app\common\model\SiteColumnModel;
use app\common\model\SiteParamModel;
use app\common\model\WsarticleModel;
use app\common\model\WscateModel;
use think\Request;

class WsdownloadController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($siteid, $columnid)
    {
        // 栏目
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);
        // 分类
        $wscate = WscateModel::getList($siteid, $columnid);
        $this->assign('wscate', $wscate);
        // 搜索表单
        $keywords = $this->request->param('keywords');
        $cateid = $this->request->param('cateid', 0);
        $this->assign('keywords', $keywords);
        $this->assign('cateid', $cateid);

        $where = array();
        $where[] = ['wsarticle_siteid', '=', $siteid];
        $where[] = ['wsarticle_columnid', '=', $columnid];
        if ($keywords != '') {
            $where[] = ['wsarticle_title', 'like', '%' . $keywords . '%'];
        }
        if ($cateid > 0) {
            $where[] = ['wsarticle_cateid', '=', $cateid];
        }
        $field = 'wsarticle_id,wsarticle_title,wsarticle_image,wsarticle_file,wsarticle_cateid,wsarticle_desc, wsarticle_home,wsarticle_recommend,wsarticle_sort,wsarticle_create_time,wsarticle_views';
        $order = 'wsarticle_sort desc, wsarticle_id desc';
        $list = WsarticleModel::where($where)->field($field)->order($order)
            ->paginate(10, false, ['query' => $this->request->param()]);
        $this->assign('list', $list);

        return $this->fetch();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        self::testPost();

        // 获取数据
        $data = $request->param();

        // 验证数据
        $validate = new WsdownloadValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 图片文件
        $image_path = $data['wsarticle_image'];
        $file_path = $data['wsarticle_file'];

        // 提交数据
        unset($data['wsarticle_id']);
        $data['wsarticle_create_time'] = '';
        $data['wsarticle_image'] = array(
            ['file_path' => $image_path, 'file_name' => 'image', 'file_size' => '0Kb', 'file_ext' => 'jpg'],
        );
        $data['wsarticle_file'] = array(
            ['file_path' => $file_path, 'file_name' => 'file', 'file_size' => '0Kb', 'file_ext' => 'txt'],
        );

        // 保存数据
        $model = WsarticleModel::create($data);
        if (!$model) {
            $this->error('提示：新增失败!');
        }

        $this->success('恭喜：新增成功!', null, '', 1);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        // 栏目
        $field = ['wsarticle_id', 'wsarticle_title', 'wsarticle_desc', 'wsarticle_image', 'wsarticle_file', 'wsarticle_cateid'];
        $data = WsarticleModel::field($field)->find($id);

        $data['wsarticle_image'] = getimg($data['wsarticle_image']);
        $data['wsarticle_file'] = getfile($data['wsarticle_file']);

        $this->success('恭喜：查询成功', null, $data, 1);
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        self::testPost();

        // 获取数据
        $data = $request->param();

        // 验证数据
        $validate = new WsdownloadValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 图片文件
        $image_path = $data['wsarticle_image'];
        $file_path = $data['wsarticle_file'];

        // 提交数据
        $data['wsarticle_update_time'] = '';
        $data['wsarticle_image'] = array(
            ['file_path' => $image_path, 'file_name' => 'image', 'file_size' => '0Kb', 'file_ext' => 'jpg'],
        );
        $data['wsarticle_file'] = array(
            ['file_path' => $file_path, 'file_name' => 'file', 'file_size' => '0Kb', 'file_ext' => 'txt'],
        );

        // 保存数据
        $model = WsarticleModel::update($data);
        if (!$model) {
            $this->error('提示：修改失败!');
        }

        $this->success('恭喜：修改成功!', null, '', 1);
    }
}
