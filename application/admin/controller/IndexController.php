<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use think\facade\Cache;
use think\facade\Env;

/**
 *
 */
class IndexController extends AdminBaseController
{
    public function index()
    {
        $setting = array();
        $setting['sys_time'] = date('Y-m-d H:i:s'); // 服务器时间
        $setting['sys_name'] = php_uname('s') . ' ' . php_uname('r'); // 操作系统
        $setting['sys_environment'] = $_SERVER["SERVER_SOFTWARE"]; // 运行环境
        $setting['php_version'] = PHP_VERSION; // PHP版本
        $setting['php_runmode'] = php_sapi_name(); // PHP 运行方式
        $setting['database_type'] = ucfirst(config('database.type')); // 数据库类型

        $setting['database_version'] = get_db_version(); // 数据库版本
        $setting['tp_version'] = \App::version(); // Tp 版本
        $setting['upload_max'] = get_upload_max(); // 最大上传
        $setting['run_time'] = ini_get('max_execution_time') . 'S'; // 最大运行时间
        $setting['sys_space'] = file_size_format(disk_free_space(Env::get('root_path'))); // 硬盘空间
        $setting['sys_path'] = Env::get('root_path'); // 硬盘空间

        $this->assign($setting);

        // 用户信息
        $manage = cookie('admin_manage');
        $this->assign('admin_manage', $manage);

        return $this->fetch();
    }

    public function cache_clear()
    {
        Cache::clear();
        $this->success('缓存清除成功！', null, '', 1);
    }

    /**
     * 退出系统
     * @return [type] [description]
     */
    public function logout()
    {
        cookie('admin_manage', null);
        $this->success('恭喜：帐号注销成功！', url('@admin/login'), '', 1);
    }
}
