<?php

namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use think\facade\Config;

/**
 *
 */
class UploadController extends AdminBaseController
{
    // 前置方法
    protected $beforeActionList = [
        'before_check_post' => ['except' => 'index,delete,storage'],
        'before_check_delete' => ['only' => 'delete'],
    ];

    public function before_check_post()
    {
        if (!$this->request->isPost()) {
            $this->error('非法操作(post)!');
        }
    }

    public function before_check_delete()
    {
        if (!$this->request->isDelete()) {
            $this->error('非法操作(delete)!');
        }
    }

    public function index()
    {
        echo 'abc';
    }

    /**
     * 文件管理
     * @return \think\Response
     */
    public function storage()
    {
        $page = $this->request->param('page', 0);
        $this->assign('page', $page);
        $folder = [];
        $files = [
            ['file-name' => '5e50e74bdc8e8', 'file-ext' => 'jpg', 'file-size' => '56Kb', 'file-path' => '/uploads/20200222/5e50e74bdc8e8.jpg'],
            ['file-name' => '5e50e74bdc8e8', 'file-ext' => 'jpg', 'file-size' => '56Kb', 'file-path' => '/uploads/20200222/5e50ec9b7f147.jpg']
        ];
        $this->assign('files', $files);
        return $this->fetch();
    }

    public function delete()
    {
        $file_path = $this->request->param('file_path');
        $file_path = '.' . ltrim($file_path, '.');
        if (file_exists($file_path)) {
            unlink($file_path);
            $this->success('恭喜：删除成功!');
        } else {
            $this->error('提示：文件不存在!');
        }
    }

    /**
     * tinymce 编辑器图片上传
     * @return [type] [description]
     */
    public function tinymce()
    {
        // 从配置信息获取上传目录
        $config = Config::pull('upload');
        $uploads_path = $config['path'];

        // 目录路径
        $uploads_path = $uploads_path . '/tinymce/' . date('Ymd');

        //文件验证
        $validate = $config['image'];

        // 获取表单上传文件 例如上传了001.jpg
        $file = $this->request->file('file');

        // 移动到框架应用根目录/uploads/ 目录下
        $info = $file->rule('uniqid')->validate($validate)->move('.' . $uploads_path);

        // dump($info);
        if ($info) {
            $myinfo = $info->getInfo();
            $data = [
                'file_name' => $myinfo['name'],
                'file_path' => trim(str_replace('\\', '/', $info->getPathName()), '.'),
                'file_size' => file_size_format($myinfo['size']),
                'file_ext' => $info->getExtension(),
            ];
            //$this->success('上传成功', null, $data, 1);
            $data = ['location' => trim(str_replace('\\', '/', $info->getPathName()), '.')];
            echo json_encode($data);
        } else {
            // 上传失败获取错误信息
            $this->error($file->getError());
        }
    }

    /**
     * image 上传图片
     * @return [type] [description]
     */
    public function image()
    {
        // 从配置信息获取上传目录
        $config = Config::pull('upload');
        $uploads_path = $config['path'];

        // 目录路径
        $uploads_path = $uploads_path . '/' . date('Ymd');

        //文件验证
        $validate = $config['image'];

        // 上传文件
        $this->upload($uploads_path, $validate);
    }

    /**
     * image 上传图片
     * @return [type] [description]
     */
    public function file()
    {
        // 从配置信息获取上传目录
        $config = Config::pull('upload');
        $uploads_path = $config['path'];

        // 目录路径
        $uploads_path = $uploads_path . '/' . date('Ymd');

        //文件验证
        $validate = $config['file'];

        // 上传文件
        $this->upload($uploads_path, $validate);
    }

    /**
     * theme 上传主题文件
     * @return [type] [description]
     */
    public function theme_file()
    {
        // 从配置信息获取上传目录
        $config = Config::pull('upload');
        $uploads_path = $config['path'];

        // 目录路径
        $uploads_path = $uploads_path . '/themes';

        //文件验证
        $validate = $config['theme'];

        // 上传文件
        $this->upload($uploads_path, $validate);
    }

    /**
     * 上传文件
     * @param  [type] $uploads_path [路径]
     * @param  array  $validate     [验证]
     * @return [type]               [description]
     */
    private function upload($uploads_path, $validate)
    {
        $path = $uploads_path;
        $vali = $validate;

        // 未安装Fileinfo扩展
        if(!function_exists('finfo_open')){
            $this->error('请先在服务器上安装 fileinfo 扩展！');
        }

        // 获取表单上传文件 例如上传了001.jpg
        $file = $this->request->file('file');

        // 移动到框架应用根目录/uploads/ 目录下
        $info = $file->rule('uniqid')->validate($vali)->move('.' . $path);

        // dump($info);
        if ($info) {
            $myinfo = $info->getInfo();
            $data = [
                'file_name' => $myinfo['name'],
                'file_path' => trim(str_replace('\\', '/', $info->getPathName()), '.'),
                'file_size' => file_size_format($myinfo['size']),
                'file_ext' => $info->getExtension(),
            ];
            $this->success('上传成功', null, $data, 1);
        } else {
            // 上传失败获取错误信息
            $this->error($file->getError());
        }
    }

}
