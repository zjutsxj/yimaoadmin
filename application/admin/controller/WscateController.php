<?php
namespace app\admin\controller;

use app\admin\validate\WscateValidate;
use app\common\controller\AdminBaseController;
use app\common\model\SiteColumnModel;
use app\common\model\WscateModel;
use think\Request;

/**
 *
 */
class WscateController extends AdminBaseController
{
    /**
     * 分类列表
     */
    public function index($siteid, $columnid)
    {
        $column = SiteColumnModel::get($columnid);
        $this->assign('column', $column);

        $keywords = $this->request->param('keywords');
        $this->assign('keywords', $keywords);

        $where = [];
        $where[] = ['wscate_siteid', '=', $siteid];
        $where[] = ['wscate_columnid', '=', $columnid];
        if ($keywords != '') {
            $where[] = ['wscate_name|wscate_title', 'like', '%' . $keywords . '%'];
        }
        $list = WscateModel::where($where)->order('wscate_sort')->select();
        $this->assign('list', $list);

        return $this->fetch();
    }

    /**
     * 设置属性
     */
    public function attr(Request $request)
    {
        self::testPost();

        $id = $request->param('id');
        $attr = $request->param('attr');
        $value = $request->param('value');
        WscateModel::where('wscate_id in (' . $id . ')')->update(['wscate_' . $attr => $value]);
        $this->success('恭喜：设置成功!', null, '', 1);
    }

    /**
     * 保存新增
     */
    public function save(Request $request)
    {
        self::testPost();

        // 获取数据
        $data = $request->param();

        // 验证数据
        $validate = new WscateValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 保存数据
        $field = ['wscate_siteid', 'wscate_columnid', 'wscate_pid', 'wscate_path', 'wscate_sort', 'wscate_name', 'wscate_title', 'wscate_page'];
        $wscate = WscateModel::create($data, $field);
        if (!$wscate) {
            $this->error('错误：新增失败!');
        }
        $this->success('恭喜：新增成功!', null, '', 1);
    }

    /**
     * 修改
     */
    public function edit($siteid, $columnid, $id)
    {
        $column = SiteColumnModel::get($columnid);
        $this->assign('column', $column);

        $wscate = WscateModel::find($id);
        $this->assign('wscate', $wscate);

        return $this->fetch();
    }

    /**
     * 保存修改
     */
    public function update(Request $request)
    {
        self::testPost();

        $data = $request->param();
        // 验证数据
        $validate = new WscateValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        if (!isset($data['wscate_image'])) {
            $data['wscate_image'] = [];
        }
        $model = WscateModel::update($data);
        if (!$model) {
            $this->error('错误：修改失败!');
        }

        $this->success('恭喜：修改成功!', $data['backurl'], '', 1);
    }

    /**
     * 删除分类
     */
    public function delete($siteid, $columnid, $id)
    {
        self::testDel();

        // 分类id， 单个id或多个id以,号分隔
        $model = WscateModel::destroy($id);

        if (!$model) {
            $this->error('错误：删除失败!');
        }

        // 更新排序号
        $model = new WscateModel;
        $model->update_sort($siteid, $columnid);

        $this->success('恭喜：删除成功!', null, '', 1);
    }

    /**
     * 栏目排序列表
     */
    public function sort($siteid, $columnid)
    {
        $column = SiteColumnModel::get($columnid);
        $this->assign('column', $column);

        $model = new WscateModel();
        $where = [];
        $where[] = ['wscate_siteid', '=', $siteid];
        $where[] = ['wscate_columnid', '=', $columnid];
        $list = $model->where($where)->order('wscate_sort asc,wscate_id asc')->select();
        // 转化为数组
        $list = $list->toArray();
        // 列表转化为树结构
        $tree = list_to_tree($list, 'wscate_id', 'wscate_pid');
        // 生成html
        $html = $model->tree_to_html($tree);
        unset($list, $tree);

        $this->assign('html', $html);
        return $this->fetch();
    }

    /**
     * 保存栏目排序
     */
    public function sort_save(Request $request)
    {
        self::testPost();

        $json = $request->param('wscate_json');
        $tree = json_decode($json, true);

        if (count($tree) == 0) {
            $this->error("提示：分类顺序没有变化，不需要保存!");
        }

        $siteid = $request->param('siteid');
        $columnid = $request->param('columnid');

        $model = new WscateModel();
        $data = $model->tree_to_data($tree);
        $model->saveAll($data);
        // 删除缓存
        $model->delete_cache($siteid, $columnid);

        $backurl = $request->param('backurl');
        $this->success('排序成功!', $backurl, '', 1);
    }

}
