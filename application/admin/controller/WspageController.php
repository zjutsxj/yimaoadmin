<?php

namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\SiteColumnModel;
use app\common\model\SiteParamModel;
use app\common\model\SiteContentModel;
use think\Request;

class WspageController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($siteid, $columnid)
    {
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);

        // 启用自定义字段
        $param = $content = [];
        if ($column['column_field'] == 1) {
            // 自定义字段
            $param = SiteParamModel::getColumnParam($siteid, $columnid);
            // 自定义字段内容
            $content = SiteContentModel::getPageContent($siteid, $columnid);
        }
        $this->assign('param', $param);
        $this->assign('content', $content);

        return $this->fetch();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function update(Request $request)
    {
        self::testPost();

        $data = $request->param();
        if (!isset($data['column_image'])) {
            $data['column_image'] = [];
        }
        $column = SiteColumnModel::update($data);
        if (!$column) {
            $this->error('提示：修改失败!');
        }

        // 保存自定义字段
        $siteid = $request->param('column_siteid');
        $columnid = $request->param('column_id');
        SiteContentModel::setPageContent($siteid, $columnid, $data);

        $this->success('恭喜：修改成功!', null, '', 1);
    }
}
