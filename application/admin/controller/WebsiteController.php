<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\SiteContentModel;
use app\common\model\SiteModel;
use app\common\model\SiteParamModel;
use think\Request;

/**
 * 网站设置
 */
class WebsiteController extends AdminBaseController
{
    public function index($siteid)
    {
        $site = SiteModel::find($siteid);
        $this->assign('site', $site);

        // 获取站点自定义字段
        $param = SiteParamModel::getSiteParam($siteid);
        $this->assign('param', $param);

        // 获取站点自定义字段的内容
        $content = SiteContentModel::getSiteContent($siteid);
        $this->assign('content', $content);

        return $this->fetch();
    }

    /**
     * 保存内容
     * @return [type] [description]
     */
    public function save(Request $request)
    {
        if (!$request->isPost()) {
            $this->error('非法操作!');
        }
        $siteid = $request->param('site_id');
        $data = [
            'site_id' => $request->param('site_id'),
            'site_home' => $request->param('site_home'),
            'site_title' => $request->param('site_title'),
            'site_keywords' => $request->param('site_keywords'),
            'site_description' => $request->param('site_description'),
            'site_jscode' => $request->param('site_jscode'),
        ];
        $site = SiteModel::update($data);
        if (!$site) {
            $this->error('提示：修改站点失败!');
        }

        // 保存站点自定义字段
        SiteContentModel::setSiteContent($siteid, $request->param());

        $this->success('恭喜：修改成功!', null, '', 1);
    }
}
