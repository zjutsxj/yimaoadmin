<?php
namespace app\admin\controller;

use app\admin\validate\ModuleValidate;
use app\common\controller\AdminBaseController;
use app\common\model\ModuleModel;
use think\Request;

/**
 *
 */
class ModuleController extends AdminBaseController
{

    public function index()
    {

        $keywords = $this->request->param('keywords');
        $this->assign('keywords', $keywords);

        $where = [];
        $where[] = ['module_id', '>=', '0'];
        if ($keywords != '') {
            $where[] = ['module_name|module_path|module_desc', 'like', '%' . $keywords . '%'];
        }
        $list = ModuleModel::where($where)->order('module_sort asc, module_id asc')->select();
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 保存新增
     * @return [type] [description]
     */
    public function save(Request $request)
    {
        self::testPost();
        // 获取数据
        $data = $request->param();
        unset($data['module_id']);

        // 验证数据
        $validate = new ModuleValidate();
        if (!$validate->sceneCreate()->check($data)) {
            $this->error($validate->getError());
        }

        // 保存数据
        $field = ['module_name', 'module_path', 'module_cate', 'module_field', 'module_desc', 'module_sort'];
        $module = ModuleModel::create($data, $field);
        if (!$module) {
            $this->error('错误：新增失败!');
        }
        $this->success('恭喜：新增成功!', null, '', 1);
    }

    /**
     * 修改
     */
    public function edit($id)
    {
        $field = ['module_id', 'module_name', 'module_path', 'module_cate', 'module_field', 'module_desc', 'module_sort'];
        $module = ModuleModel::field($field)->find($id);
        $this->success('恭喜：查询成功', null, $module, 1);
    }

    /**
     * 保存修改
     */
    public function update(Request $request)
    {
        self::testPost();
        $data = $request->param();
        // 验证数据
        $validate = new ModuleValidate();
        if (!$validate->sceneUpdate()->check($data)) {
            $this->error($validate->getError());
        }

        $module = ModuleModel::update($data);

        if (!$module) {
            $this->error('错误：修改失败!');

        }

        $this->success('恭喜：修改成功!', null, '', 1);
    }

    /**
     * 删除
     */
    public function delete(Request $request, $id)
    {
        self::testDel();
        // $id 数据格式
        // $id：1
        // $id：1，2，3
        $module = ModuleModel::destroy($id);

        if (!$module) {
            $this->error('错误：删除失败!');
        }

        $this->success('恭喜：删除成功!', null, '', 1);
    }

    /**
     * [sort 更新排序]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function sort(Request $request)
    {
        self::testPost();

        $id = $request->param('id', 0);
        $sort = $request->param('sort', 0);

        $data['module_id'] = $id;
        $data['module_sort'] = $sort;

        ModuleModel::update($data);

        $this->success('排序成功!', null, ['jump' => 'no'], 1);
    }

}
