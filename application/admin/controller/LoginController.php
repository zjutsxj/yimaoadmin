<?php

namespace app\admin\controller;

use app\admin\validate\LoginValidate;
use app\common\controller\BaseController;
use app\common\model\ManageModel;
use think\captcha\Captcha;

class LoginController extends BaseController
{
    /**
     * 登录页面
     * @return \think\Response
     */
    public function index()
    {
        $username = '';
        $password = '';
        $remember = false;
        if (cookie('?admin_remember')) {
            $data = cookie('admin_remember');
            $data = unserialize($data);
            $username = trim(encrypt_decrypt('yimao2020', $data['username'], 1));
            $password = trim(encrypt_decrypt('yimao2020', $data['password'], 1));
            $remember = true;
        }
        $this->assign('username', $username);
        $this->assign('password', $password);
        $this->assign('remember', $remember);

        $this->assign('setting', config('setting.'));

        return $this->fetch();
    }

    /**
     * 验证码
     * @return [type] [description]
     */
    public function verify()
    {
        $captcha = new Captcha();
        $captcha->fontSize = 14;
        $captcha->length = 4;
        $captcha->useCurve = false;
        $captcha->useNoise = false;
        $captcha->imageH = 32;
        return $captcha->entry();
    }

    /**
     * 登录帐号
     * @return [type] [description]
     */
    public function dologin()
    {
        self::testPost();

        $data = $this->request->param();

        // 验证数据
        $validate = new LoginValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 登录系统
        $manage = new ManageModel;
        $result = $manage->do_login($data['login_username'], $data['login_password']);
        if (!$result) {
            $this->error($manage->getError());
        }

        // 登录状态
        if ($this->request->param('login_remember')) {
            $username = $this->request->param('login_username');
            $password = $this->request->param('login_password');
            $data = [
                'username' => encrypt_decrypt('yimao2020', $username),
                'password' => encrypt_decrypt('yimao2020', $password),
            ];
            cookie('admin_remember', serialize($data));
        } else {
            cookie('admin_remember', null);
        }
        $this->success('登录成功！', url('@admin'), null, 1);
    }

    /**
     * 忘记密码
     * @return [type] [description]
     */
    public function doforget()
    {
        self::testPost();

        $code = $this->request->param('forget_code');
        $username = $this->request->param('forget_username');
        if ($username == '') {
            $this->error('提示：手机/邮箱必须填写!');
        }
        if (!captcha_check($code)) {
            $this->error('提示：验证码错误!');
        }
        if (!chk_mobile($username) && !chk_email($username)) {
            $this->error('提示：必须是手机或邮箱!');
        }

        $data = ['phone' => '13588254045', 'text' => '手机短信内容'];
        $result = Hook::listen('send_msg', $data);

        $this->error('帐号不存在！');
    }
}
