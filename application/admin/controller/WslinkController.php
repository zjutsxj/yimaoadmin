<?php

namespace app\admin\controller;

use app\admin\validate\WslinkValidate;
use app\common\controller\AdminBaseController;
use app\common\model\SiteColumnModel;
use app\common\model\WsarticleModel;
use think\Request;

class WslinkController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($siteid, $columnid)
    {
        $param = $this->request->param();
        // 栏目
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);
        // 搜索表单
        $keywords = $this->request->param('keywords');
        $this->assign('keywords', $keywords);

        $where = array();
        $where[] = ['wsarticle_siteid', '=', $siteid];
        $where[] = ['wsarticle_columnid', '=', $columnid];
        if ($keywords != '') {
            $where[] = ['wsarticle_title', 'like', '%' . $keywords . '%'];
        }
        $field = 'wsarticle_id, wsarticle_title, wsarticle_image, wsarticle_sort,wsarticle_desc,wsarticle_page';
        $order = 'wsarticle_sort desc, wsarticle_id desc';
        $list = WsarticleModel::where($where)->field($field)->order($order)
            ->paginate(10, false, ['query' => $param]);
        $this->assign('list', $list);

        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function edit($id)
    {
        $field = ['wsarticle_id', 'wsarticle_title', 'wsarticle_desc', 'wsarticle_image', 'wsarticle_page'];
        $data = WsarticleModel::field($field)->find($id);

        $data['wsarticle_image'] = getimg($data['wsarticle_image']);

        $this->success('恭喜：查询成功', null, $data, 1);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        self::testPost();

        // 获取数据
        $data = $request->param();

        // 验证数据
        $validate = new WslinkValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 提交数据
        $data['wsarticle_create_time'] = '';
        // logo
        if ($data['wsarticle_image'] != '') {
            $data['wsarticle_image'] = array(
                0 => [
                    'file_path' => $data['wsarticle_image'],
                    'file_name' => 'banner',
                    'file_size' => '0Kb',
                    'file_ext' => 'jpg',
                ],
            );
        } else {
            $data['wsarticle_image'] = array();
        }

        // 保存数据
        $model = WsarticleModel::create($data);
        if (!$model) {
            $this->error('提示：新增失败!');
        }

        $this->success('恭喜：新增成功!', null, '', 1);
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        self::testPost();

        // 获取数据
        $data = $request->param();

        // 验证数据
        $validate = new WslinkValidate();
        if (!$validate->check($data)) {
            $this->error($validate->getError());
        }

        // 数据
        $data['wsarticle_update_time'] = '';
        // logo
        if ($data['wsarticle_image'] != '') {
            $data['wsarticle_image'] = array(
                0 => [
                    'file_path' => $data['wsarticle_image'],
                    'file_name' => 'banner',
                    'file_size' => '0Kb',
                    'file_ext' => 'jpg',
                ],
            );
        } else {
            $data['wsarticle_image'] = array();
        }

        // 保存数据
        $model = WsarticleModel::update($data);
        if (!$model) {
            $this->error('提示：修改失败!');
        }

        $this->success('恭喜：修改成功!', null, '', 1);
    }
}
