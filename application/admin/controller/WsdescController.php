<?php

namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\SiteColumnModel;
use think\Request;

class WsdescController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($siteid, $columnid)
    {
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);
        return $this->fetch();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function update(Request $request)
    {
        self::testPost();

        $data = $request->param();
        $column = SiteColumnModel::update($data);
        if (!$column) {
            $this->error('提示：修改失败!');
        }
        $this->success('恭喜：修改成功!', null, '', 1);
    }
}
