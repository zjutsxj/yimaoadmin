<?php

namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\SiteColumnModel;
use app\common\model\WsmessageModel;
use think\Request;

class WsmessageController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($siteid, $columnid)
    {
        $param = $this->request->param();
        // 栏目
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);
        // 搜索表单
        $keywords = $this->request->param('keywords');
        $this->assign('keywords', $keywords);
        $status = $this->request->param('status', '');
        $this->assign('status', $status);

        $where = array();
        $where[] = ['wsmessage_siteid', '=', $siteid];
        $where[] = ['wsmessage_columnid', '=', $columnid];
        if ($keywords != '') {
            $where[] = ['wsmessage_name|wsmessage_phone|wsmessage_email|wsmessage_desc', 'like', '%' . $keywords . '%'];
        }
        if ($status != '') {
            $where[] = ['wsmessage_status', '=', $status];
        }
        $order = 'wsmessage_status asc, wsmessage_id desc';
        $list = WsmessageModel::where($where)->order($order)
            ->paginate(10, false, ['query' => $param]);
        $this->assign('list', $list);

        return $this->fetch();
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update($id)
    {
        self::testGet();
        // 数据
        $ids = explode(',', $id);
        $data = array();
        foreach ($ids as $key => $id) {
            $data[] = [
                'wsmessage_id' => $id,
                'wsmessage_status' => 1,
                'wsmessage_update_time' => '',
            ];
        }

        // 保存数据
        $model = new WsmessageModel;
        $model->saveAll($data);
        if (!$model) {
            $this->error('提示：确认失败!');
        }

        $this->success('恭喜：确认成功!', null, '', 1);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        self::testDel();

        $model = WsmessageModel::destroy($id);
        if (!$model) {
            $this->error('错误：删除失败!');
        }
        $this->success('恭喜：删除成功!', null, '', 1);
    }
}
