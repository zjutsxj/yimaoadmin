<?php

namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\SiteColumnModel;
use think\Request;

class WscolumnController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($siteid, $columnid)
    {
        $column = SiteColumnModel::find($columnid);
        $this->assign('column', $column);
        $this->assign('backurl', $_SERVER['HTTP_REFERER']);
        return $this->fetch();
    }

    /**
     * 保存更新的资源
     */
    public function update(Request $request)
    {
        self::testPost();

        $data = $request->param();
        if (!isset($data['column_image'])) {
            $data['column_image'] = [];
        }
        $column = SiteColumnModel::update($data);
        if (!$column) {
            $this->error('提示：修改失败!');
        }
        $backurl = $request->param('backurl');
        $this->success('恭喜：修改成功!', $backurl, '', 1);
    }
}
