<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;

/**
 *
 */
class HelpController extends AdminBaseController
{
    public function index()
    {
        return $this->fetch();
    }

    public function pjax()
    {
        return $this->fetch();
    }

    public function pjax_list()
    {
        $this->assign('id', $this->request->param('id', 0));
        return $this->fetch();
    }

    public function upload()
    {
        return $this->fetch();
    }

    public function tinymce()
    {
        return $this->fetch();
    }

    public function _empty()
    {
        return $this->fetch();
    }
}
