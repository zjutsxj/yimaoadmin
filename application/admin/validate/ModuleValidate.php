<?php

namespace app\admin\validate;

use think\Validate;

class ModuleValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'module_name' => 'require',
        'module_path' => 'require|alphaNum|unique:module',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'module_name.require' => '名称必须填写',
        'module_path.require' => '路径必须填写',
        'module_path.alphaNum' => '路径必须为字母数字',
        'module_path.unique' => '路径已经存在',
    ];

    // create 验证场景定义
    public function sceneCreate()
    {
        return $this->only(['module_name', 'module_path']);
    }

    // update 验证场景定义
    public function sceneUpdate()
    {
        return $this->only(['module_name', 'module_path'])
            ->remove('module_path', 'unique')
            ->append('module_path', 'unique:module, module_path, module_id');
    }
}
