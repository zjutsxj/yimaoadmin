<?php

namespace app\admin\validate;

use think\Validate;

class SiteValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'site_name' => 'require',
        'site_mark' => 'require|alpha|unique:site'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'site_name.require' => '名称必须填写',
        'site_mark.require' => '标识必须填写',
        'site_mark.alpha' => '标识必须为英文字母',
        'site_mark.unique' => '标识已经存在'
    ];

    // create 验证场景定义
    public function sceneCreate()
    {
        return $this->only(['site_name', 'site_mark']);
    }

    // update 验证场景定义
    public function sceneUpdate()
    {
        return $this->only(['site_name', 'site_mark'])
            ->remove('site_mark', 'unique')
            ->append('site_mark', 'unique:site, site_mark, site_id');
    }
}
