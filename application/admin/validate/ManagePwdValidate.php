<?php

namespace app\admin\validate;

use think\Validate;

class ManagePwdValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'old_password' => 'require|min:5',
        'new_password' => 'require|min:5|confirm:agn_password',
        'agn_password' => 'require|min:5',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'old_password.require' => '原始密码必须填写',
        'old_password.min' => '原始密码至少5个字符',
        'new_password.require' => '新密码必须填写',
        'new_password.min' => '新密码至少5个字符',
        'new_password.confirm' => '二个密码不一致',
        'agn_password.require' => '重置密码必须填写',
        'agn_password.min' => '重置密码至少5个字符',
    ];
}
