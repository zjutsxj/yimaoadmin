<?php

namespace app\admin\validate;

use think\Validate;

class LoginValidate extends Validate
{

    protected $rule = [
        'login_username' => 'require|min:5|max:25|alphaNum',
        'login_password' => 'require',
    ];

    protected $message = [
        'login_username.require' => '帐号必须填写',
        'login_username.min' => '帐号至少5个字符',
        'login_username.max' => '帐号不要超过25个字符',
        'login_username.alphaNum' => '帐号只能字符、数字组成',
        'login_password.require' => '密码必须填写',
    ];
}
