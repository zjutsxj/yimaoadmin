<?php

namespace app\admin\validate;

use think\Validate;

class WscateValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'wscate_siteid' => 'require|number',
        'wscate_columnid' => 'require|number',
        'wscate_name' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'wscate_siteid.require' => 'siteid必须填写',
        'wscate_siteid.number' => 'siteid必须数字',
        'wscate_columnid.require' => 'columnid必须填写',
        'wscate_columnid.number' => 'columnid必须数字',
        'wscate_name.require' => '名称必须填写',
    ];
}
