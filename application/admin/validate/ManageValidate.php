<?php

namespace app\admin\validate;

use think\Validate;

class ManageValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'manage_username' => 'require|min:5|max:25|alphaNum|unique:manage',
        'manage_fullname' => 'require',
        'manage_phone' => 'mobile|unique:manage',
        'manage_email' => 'email|unique:manage',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'manage_username.require' => '帐号必须填写',
        'manage_username.min' => '帐号至少5个字符',
        'manage_username.max' => '帐号不能超过25个字符',
        'manage_username.alphaNum' => '帐号只能数字、字母组成',
        'manage_username.unique' => '帐号已经存在',
        'manage_fullname.require' => '姓名必须填写',
        'manage_phone.mobile' => '手机格式错误',
        'manage_phone.unique' => '手机已经存在',
        'manage_email.email' => '邮箱格式错误',
        'manage_email.unique' => '邮箱已经存在',
    ];

    // create 验证场景定义
    public function sceneCreate()
    {
        return $this->only(['manage_username', 'manage_fullname', 'manage_phone', 'manage_email']);
    }

    // update 验证场景定义
    public function sceneUpdate()
    {
        return $this->only(['manage_username', 'manage_fullname', 'manage_phone', 'manage_email'])
            ->remove('manage_username', 'unique')
            ->append('manage_username', 'unique:manage, manage_username, manage_id')
            ->remove('manage_phone', 'unique')
            ->append('manage_phone', 'unique:manage, manage_phone, manage_id')
            ->remove('manage_email', 'unique')
            ->append('manage_email', 'unique:manage, manage_email, manage_id');
    }

    /**
     * 用户登录
     * @return [type] [description]
     */
    public function sceneLogin()
    {

    }
}
