<?php

namespace app\admin\validate;

use think\Validate;

class WsjobsValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'wsarticle_siteid' => 'require|number',
        'wsarticle_columnid' => 'require|number',
        'wsarticle_title' => 'require',
        'wsarticle_page' => 'alphaDash',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'wsarticle_siteid.require' => 'siteid必须填写',
        'wsarticle_siteid.number' => 'siteid必须数字',
        'wsarticle_columnid.require' => 'columnid必须填写',
        'wsarticle_columnid.number' => 'columnid必须数字',
        'wsarticle_title.require' => '标题必须填写',
        'wsarticle_page.alphaDash' => '字母、数字、下划线、破折号',
    ];
}
