<?php

namespace app\admin\validate;

use think\Validate;

class ManageInfoValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'manage_fullname' => 'require',
        'manage_phone' => 'mobile|unique:manage',
        'manage_email' => 'email|unique:manage',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'manage_fullname.require' => '姓名必须填写',
        'manage_phone.mobile' => '手机格式错误',
        'manage_phone.unique' => '手机已经存在',
        'manage_email.email' => '邮箱格式错误',
        'manage_email.unique' => '邮箱已经存在',
    ];
}
