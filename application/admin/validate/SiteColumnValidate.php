<?php

namespace app\admin\validate;

use think\Validate;

class SiteColumnValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'column_siteid' => 'require|number',
        'column_name' => 'require',
        'column_enname' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'column_siteid.require' => 'siteid必须填写',
        'column_siteid.number' => 'siteid必须数字',
        'column_name.require' => '名称必须填写',
        'column_enname.require' => '英文名称必须填写',
    ];
}
