<?php

namespace app\admin\validate;

use think\Validate;

class SiteParamValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'param_name' => 'require',
        'param_type' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'param_name.require' => '名称必须填写',
        'param_type.require' => '参数类型必须选择',
    ];

    // create 验证场景定义
    public function sceneCreate()
    {
        return $this->only(['param_name', 'param_type']);
    }

    // update 验证场景定义
    public function sceneUpdate()
    {
        return $this->only(['param_name', 'param_type']);
    }
}
