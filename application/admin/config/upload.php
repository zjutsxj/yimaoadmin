<?php

/** upload 文件上传配置 **/
return array(
    'path' => '/uploads',
    'image' => [
        'size' => 10 * 1024 * 1024,
        'ext' => 'jpg,jpeg,png,gif',
        'type' => 'image/pjpeg,image/jpeg,image/png,image/x-png,image/gif,image/x-gif',
    ],
    'file' => [
        'size' => 10 * 1024 * 1024,
        'ext' => 'txt,zip,gzip,pdf',
        'type' => 'text/plain,application/zip,application/x-zip,application/gzip,application/x-zip-compressed,application/pdf',
    ],
    'theme' => [
        'size' => 10 * 1024 * 1024,
        'ext' => 'zip',
        'type' => 'application/zip,application/x-zip,application/gzip,application/x-zip-compressed',
    ],

);
