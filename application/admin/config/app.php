<?php 
/** admin 模块文件配置 **/
return array (
  'url_common_param' => true,
  'version' => '3.0.5',
  'superid' => '1,2',
  'themes_path' => '/themes',
  'themes_mark' => 'sample',
  'databk_path' => '/uploads/databk',
  'param_type' => 
  array (
    'text' => '单行文本框',
    'number' => '数字文本框',
    'email' => '邮箱文本框',
    'textarea' => '多行文本框',
    'select' => '下拉框',
    'checkbox' => '多选框',
    'radio' => '单选框',
    'image' => '图片上传',
    'file' => '附件上传',
    'editor' => '编辑器',
  ),
);