/*
Navicat MySQL Data Transfer

Source Server         : 00本地数据库列表
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : zysj_site

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2021-06-19 20:31:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `site_manage`
-- ----------------------------
DROP TABLE IF EXISTS `site_manage`;
CREATE TABLE `site_manage` (
  `manage_id` int(11) NOT NULL AUTO_INCREMENT,
  `manage_groupid` int(11) NOT NULL DEFAULT '0' COMMENT '用户组',
  `manage_username` varchar(50) NOT NULL DEFAULT '' COMMENT '帐号',
  `manage_password` varchar(100) NOT NULL DEFAULT '' COMMENT '密码',
  `manage_fullname` varchar(50) DEFAULT '' COMMENT '姓名',
  `manage_phone` varchar(20) DEFAULT '' COMMENT '手机',
  `manage_email` varchar(100) DEFAULT '' COMMENT '邮箱',
  `manage_login_num` int(11) DEFAULT '0' COMMENT '登录次数',
  `manage_login_time` int(11) DEFAULT '0' COMMENT '最后一次登录时间',
  `manage_last_time` int(11) DEFAULT '0',
  `manage_login_ip` varchar(20) DEFAULT '' COMMENT '登录ip',
  `manage_status` tinyint(4) DEFAULT '0' COMMENT '状态\r\n-1:删除\r\n0:禁用\r\n1:正常\r\n2:待审核',
  `manage_create_time` int(11) DEFAULT '0' COMMENT '创意时间',
  `manage_update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`manage_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_manage
-- ----------------------------
INSERT INTO `site_manage` VALUES ('1', '1', 'zjutsxj', '$2y$10$L0egXCE3HYbjSTY.vcJzQuAV/Lbgm.JYf5FpnkbXiotLQKm9hntRi', 'Yimao软件', '13588254045', 'zjutsxj@qq.com', '57', '1623655184', '1623655155', '127.0.0.1', '1', '1582026395', '1623655184');
INSERT INTO `site_manage` VALUES ('2', '1', 'xiaowu34', '$2y$10$JKfRepUJhtegLTj5DHsHNOZq/AMTssWDnE4Nt51iW98DFQzUT5ziu', '小吴', '13588888888', 'zjutsxjabc@qq.com', '0', '0', '0', null, '1', '1582026596', '1582215194');
INSERT INTO `site_manage` VALUES ('14', '1', 'admin', '$2y$10$xFFwUOHSVpFHr3CjQ/uCn.xHrTJaEroSgunZRr.Ej4Cl9MWucPmF2', '管理员', '13588254041', 'zjutsxj@qq111.com', '38', '1623022533', '1600648558', '127.0.0.1', '1', '1583583071', '1623022533');

-- ----------------------------
-- Table structure for `site_manage_group`
-- ----------------------------
DROP TABLE IF EXISTS `site_manage_group`;
CREATE TABLE `site_manage_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) DEFAULT '',
  `group_sort` int(11) DEFAULT '0' COMMENT '排序',
  `group_action` text COMMENT '功能',
  `group_create_time` int(11) DEFAULT '0',
  `group_update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_manage_group
-- ----------------------------
INSERT INTO `site_manage_group` VALUES ('1', '超级管理员', '0', '1-0,1-51,1-48,1-46,1-49,1-45,1-58,1-53,1-54,1-55,1-56,1-47,1-57,1-52,3-0,3-59,setting-index,setting-info,setting-db,manage-index,manage-create,manage-edit,manage-delete,manage-reset,manage-checked,manage-disable,manage-group,group-create,group-edit,group-delete,group-action,databk-index,databk-create,databk-download,databk-delete,theme-index,theme-upload,theme-delete,theme-switch,site-index,site-create,site-param,site-edit,site-delete,site-column,column-create,column-edit,column-delete,column-param,module-index,module-create,module-edit,module-delete,plugin-index,plugin-create,plugin-edit,plugin-delete', '1582263621', '1622985852');
INSERT INTO `site_manage_group` VALUES ('2', '管理员', '0', '1-0,1-51,1-48,1-49,1-46,1-47,1-45,1-52,1-53,1-54,1-55,1-56,1-57,3-0,setting-index,setting-info,setting-db,manage-index,manage-create,manage-edit,manage-delete,manage-reset,manage-checked,manage-disable,manage-group,group-edit,group-delete,group-action,databk-index,databk-create,databk-download,databk-delete,theme-index,theme-upload,theme-delete,theme-switch,site-index,site-create,site-param,site-edit,site-delete,site-column,column-create,column-edit,column-delete,column-param,module-index,module-create,module-edit,module-delete,plugin-index,plugin-create,plugin-edit,plugin-delete', '1582264022', '1585019871');
INSERT INTO `site_manage_group` VALUES ('3', '制作人员', '0', '1-0,1-51,1-48,1-49,1-46,1-47,1-45,1-52,1-53,1-54,1-55,1-56,1-57,3-0', '1582264030', '1585020049');
INSERT INTO `site_manage_group` VALUES ('4', '测试人员', '0', '1-0,1-51,1-48,1-46,1-49,1-45,1-58,1-53,1-54,1-55,1-56,1-47,1-57,1-52,3-0,3-59,setting-index,setting-info,setting-db,manage-index,manage-create,manage-edit,manage-delete,manage-reset,manage-checked,manage-disable,manage-group,group-create,group-edit,group-delete,group-action,databk-index,databk-create,databk-download,databk-delete,theme-index,theme-upload,theme-delete,theme-switch,site-index,site-create,site-param,site-edit,site-delete,site-column,column-create,column-edit,column-delete,column-param,module-index,module-create,module-edit,module-delete,plugin-index,plugin-create,plugin-edit,plugin-delete', '1582264037', '1585030666');
INSERT INTO `site_manage_group` VALUES ('5', '其它人员', '0', '', '1582264046', '1585029770');
INSERT INTO `site_manage_group` VALUES ('6', '写作人员', '0', '', '1582264053', '1622985871');

-- ----------------------------
-- Table structure for `site_module`
-- ----------------------------
DROP TABLE IF EXISTS `site_module`;
CREATE TABLE `site_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT '',
  `module_path` varchar(255) DEFAULT '' COMMENT '模型路径',
  `module_cate` tinyint(4) DEFAULT '0' COMMENT '支持分类',
  `module_field` tinyint(4) DEFAULT '0' COMMENT '自定义字段',
  `module_desc` varchar(255) DEFAULT '',
  `module_sort` int(11) DEFAULT '0' COMMENT '排序',
  `module_create_time` int(11) DEFAULT '0',
  `module_update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_module
-- ----------------------------
INSERT INTO `site_module` VALUES ('1', '单页内容', 'wspage', '0', '1', '编辑器，可设置标题、关键词、描述等网页优化内容', '1', '1582553335', '1623666941');
INSERT INTO `site_module` VALUES ('2', '描述模块', 'wsdesc', '0', '0', '编辑器，无标题、关键词、描述等网页优化内容。', '2', '1582553860', '1623672699');
INSERT INTO `site_module` VALUES ('3', 'Banner功能	', 'wsbanner', '0', '1', 'Banner功能模块	', '3', '1582555025', '1623672705');
INSERT INTO `site_module` VALUES ('5', '文章列表', 'wsarticle', '1', '1', '列表支持图片,文章列表', '4', '1582555463', '1584682263');
INSERT INTO `site_module` VALUES ('6', '产品列表', 'wsproducts', '1', '1', '图片功能为主导，类似文章。主要应用于产品列表，有分类功能，多图展示比图片列表功能更加强大', '5', '1582555494', '1584682264');
INSERT INTO `site_module` VALUES ('7', '图片列表', 'wsphoto', '1', '1', '类似相册/支持图片分类，只有图片标题，图片描述', '6', '1582555601', '1623677972');
INSERT INTO `site_module` VALUES ('9', '下载模块', 'wsdownload', '1', '0', '下载模块，文档下载(rar,zip,pdf,txt)这几种类型', '7', '1582555965', '1584682269');
INSERT INTO `site_module` VALUES ('11', '人才招聘	', 'wsjobs', '1', '1', '人才招聘(功能包括人才招聘资料、简历提交等)', '9', '1582556056', '1584682287');
INSERT INTO `site_module` VALUES ('12', '留言模块', 'wsmessage', '0', '0', '单独调用的网站留言功能(姓名,电话,留言内容)', '11', '1582556107', '1584682292');
INSERT INTO `site_module` VALUES ('13', '友情链接', 'wslink', '0', '0', '友情链接功能(标题,链接,描述,logo)', '8', '1582556134', '1584682281');

-- ----------------------------
-- Table structure for `site_site`
-- ----------------------------
DROP TABLE IF EXISTS `site_site`;
CREATE TABLE `site_site` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(50) DEFAULT '',
  `site_mark` varchar(50) DEFAULT '' COMMENT '站点标识',
  `site_domain` varchar(100) DEFAULT '' COMMENT '域名',
  `site_default` tinyint(4) DEFAULT '0',
  `site_desc` varchar(255) DEFAULT '',
  `site_sort` int(11) DEFAULT '0' COMMENT '排序',
  `site_create_time` int(11) DEFAULT '0',
  `site_update_time` int(11) DEFAULT '0',
  `site_home` varchar(255) DEFAULT '' COMMENT '首页标题',
  `site_title` varchar(255) DEFAULT '' COMMENT '站点名称',
  `site_keywords` varchar(255) DEFAULT '' COMMENT '关键词',
  `site_description` text COMMENT '描述',
  `site_jscode` text COMMENT '代码',
  PRIMARY KEY (`site_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_site
-- ----------------------------
INSERT INTO `site_site` VALUES ('1', '网站内容', 'cn', '', '1', '网站内容123', '1', '1582533386', '1622994143', '网站制作，网站优化，系统开发 - ', '杭州品宣电子商务有限公司', '网站制作，网站优化，系统开发s ', '杭州品宣电子商务有限公司网站制作，网站优化，系统开发 057188888888', '<script>var url = \"baidu.com\";</script>');
INSERT INTO `site_site` VALUES ('3', '英文网站', 'en', '', '0', '英文网站', '5', '1582538073', '1622553216', 'english', '网站名称 ( website name )', '关键词', '内容', '代码');

-- ----------------------------
-- Table structure for `site_site_column`
-- ----------------------------
DROP TABLE IF EXISTS `site_site_column`;
CREATE TABLE `site_site_column` (
  `column_id` int(11) NOT NULL AUTO_INCREMENT,
  `column_siteid` int(11) DEFAULT '0' COMMENT '站点ID',
  `column_pid` int(11) DEFAULT '0',
  `column_path` varchar(255) DEFAULT '',
  `column_child` tinyint(4) DEFAULT '0',
  `column_icon` varchar(50) DEFAULT '',
  `column_moduleid` int(11) DEFAULT '0' COMMENT '模块',
  `column_name` varchar(255) DEFAULT '' COMMENT '栏目名称',
  `column_enname` varchar(255) DEFAULT '',
  `column_sort` int(11) DEFAULT '0' COMMENT '排序',
  `column_cate` tinyint(4) DEFAULT '0' COMMENT '开启分类',
  `column_field` tinyint(4) DEFAULT '0' COMMENT '开启字段自定义',
  `column_title` varchar(255) DEFAULT '',
  `column_keywords` varchar(255) DEFAULT '',
  `column_description` varchar(255) DEFAULT '',
  `column_details` text,
  `column_image` text COMMENT '图片',
  `column_file` text COMMENT '文件',
  `column_create_time` int(11) DEFAULT '0',
  `column_update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`column_id`),
  KEY `column_id` (`column_id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_site_column
-- ----------------------------
INSERT INTO `site_site_column` VALUES ('51', '1', '0', '0', '0', 'icon-picture', '3', '网站Banner', 'description', '1', '0', '0', '网站Banner', '', '', null, null, null, '1584332672', '1623809820');
INSERT INTO `site_site_column` VALUES ('52', '1', '0', '0', '0', 'icon-link', '13', '友情链接', 'description', '13', '0', '0', '友情链接', '', '', null, null, null, '1584364365', '1623331091');
INSERT INTO `site_site_column` VALUES ('53', '1', '0', '0', '0', 'icon-book', '6', '产品列表', 'description', '7', '2', '0', '产品列表', '产品列表', '产品列表', null, 'a:0:{}', null, '1584374130', '1623331091');
INSERT INTO `site_site_column` VALUES ('45', '1', '0', '0', '0', 'icon-check-empty', '5', '公司新闻', 'description', '5', '2', '1', '公司新闻', '专注企业官网建设，功能完备，操作方便，全平台优雅体验 ', '专注企业官网建设，功能完备，操作方便，全平台优雅体验 ', '<p>这里介绍该栏目的主要内容，可以对该进行SEO优化的内容</p>', 'a:1:{i:101;a:4:{s:9:\"file_name\";s:8:\"banner01\";s:9:\"file_path\";s:35:\"/uploads/20200314/5e6ce0d205e37.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:8:\"346.27KB\";}}', null, '1583677910', '1623331091');
INSERT INTO `site_site_column` VALUES ('46', '1', '48', '0,48', '0', 'icon-file-text', '1', '公司简介', 'description', '3', '0', '0', '公司简介', '', '', '<h2>系统介绍</h2><ul class=\" list-paddingleft-2\" style=\"list-style-type: circle;\"><li><p class=\"f12\">YimaoAdmin v3.0.0 企业建站系统，使用 thinkphp5.1.27 + mysql 开发。&nbsp;&nbsp;</p></li><li><p>php&nbsp;要求&nbsp;5.6&nbsp;以上版本，推荐使用&nbsp;5.6,&nbsp;7.0,&nbsp;7.1，扩展(curl,json,pdo,gd,filter,rewrite)</p></li><li><p>数据库：mysql&nbsp;推荐使用5.5/5.6</p></li><li><p>服务器软件apache或者nginx</p></li></ul><h2>系统特点<br/></h2><p>1.&nbsp;功能模块丰富，功能模块支持无限级分类、支持参数自定义;<br/>2.&nbsp;添加无限多个语言版本，各个版本之间单独设置互不影响;</p><h2><a id=\"_15\"></a>使用说明</h2><p>如果你有 thinkphp5.1.*&nbsp;&nbsp;的开发经验那你可以轻而易举的使用本系统来制作建企业网站，如果你对 thinkphp 不了解也没关系，只要你会制作 html 页面也可以很方便创建企业网站。</p>', 'a:2:{i:101;a:4:{s:9:\"file_name\";s:13:\" yimao 软件\";s:9:\"file_path\";s:35:\"/uploads/20200314/5e6ced8e175bf.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:8:\"346.27KB\";}i:102;a:4:{s:9:\"file_name\";s:13:\" yimao 软件\";s:9:\"file_path\";s:35:\"/uploads/20200620/5eece5e34ecf3.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:6:\"96.3KB\";}}', null, '1583738845', '1623809997');
INSERT INTO `site_site_column` VALUES ('47', '1', '0', '0', '0', 'icon-file-text', '1', '联系我们', 'description', '11', '0', '0', '联系我们', '关于我们', '', '<h2><span style=\"font-size: 14px;\">联系我们</span><br/></h2><p style=\"text-indent: 2em;\">可以通过以下方式联系我们</p><p>QQ：425225349sss</p><p style=\"text-indent: 2em;\"><span style=\"font-family: &quot;arial black&quot;, &quot;avant garde&quot;;\">Pellentesque habitant morbi tristique <span style=\"color: rgb(224, 62, 45);\"><strong>senectus et netus et malesuada fames</strong></span> ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. <span style=\"text-decoration: underline;\"><strong>Ut felis.</strong></span></span></p>', 'a:2:{i:101;a:4:{s:9:\"file_name\";s:19:\"8789695741008497420\";s:9:\"file_path\";s:35:\"/uploads/20200314/5e6cb9bac7737.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"37.64KB\";}i:102;a:4:{s:9:\"file_name\";s:10:\"case00.jpg\";s:9:\"file_path\";s:35:\"/uploads/20200314/5e6cdea0e1d7a.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"40.68KB\";}}', null, '1583752877', '1623331091');
INSERT INTO `site_site_column` VALUES ('48', '1', '0', '0', '1', 'icon-file-text', '0', '公司信息', 'description', '2', '0', '0', '公司信息', '', '', null, null, null, '1583753058', '1623331091');
INSERT INTO `site_site_column` VALUES ('57', '1', '0', '0', '0', 'icon-comments', '12', '在线留言', 'description', '12', '0', '0', '在线留言', '', '', null, null, null, '1584702749', '1623331091');
INSERT INTO `site_site_column` VALUES ('54', '1', '0', '0', '0', 'icon-calendar', '7', '图片列表', 'description', '8', '1', '1', '图片列表', '', '', null, null, null, '1584374173', '1623677993');
INSERT INTO `site_site_column` VALUES ('55', '1', '0', '0', '0', 'icon-download-alt', '9', '下载中心', 'description', '9', '1', '0', '下载中心', '', '', null, null, null, '1584374221', '1623331091');
INSERT INTO `site_site_column` VALUES ('56', '1', '0', '0', '0', 'icon-renren', '11', '人才招聘', 'description', '10', '1', '1', '人才招聘', '', '', null, null, null, '1584702720', '1623331091');
INSERT INTO `site_site_column` VALUES ('49', '1', '48', '0,48', '0', 'icon-file-text', '2', '企业文化', 'description', '4', '0', '0', '企业文化', '', '', '<p>企业文化内容详细内容</p>', 'a:0:{}', null, '1583754801', '1623331091');
INSERT INTO `site_site_column` VALUES ('58', '1', '0', '0', '0', 'icon-calendar', '5', '文章列表', 'description', '6', '1', '0', '文章列表', '', '', null, null, null, '1589162521', '1623331091');
INSERT INTO `site_site_column` VALUES ('59', '3', '0', '0', '0', 'icon-list-ul', '0', '测试内容', '测试内容', '1', '0', '0', '', '', '', null, null, null, '1592742712', '1622553223');
INSERT INTO `site_site_column` VALUES ('64', '1', '0', '0', '0', 'icon-th-large', '5', '无类新闻', 'description', '14', '0', '0', '无类新闻', '', '', null, null, null, '1623331091', '1623331091');

-- ----------------------------
-- Table structure for `site_site_content`
-- ----------------------------
DROP TABLE IF EXISTS `site_site_content`;
CREATE TABLE `site_site_content` (
  `content_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content_siteid` int(11) DEFAULT '0' COMMENT '站点ID',
  `content_columnid` int(11) DEFAULT '0' COMMENT '栏目ID',
  `content_paramid` int(11) DEFAULT '0' COMMENT '字段ID',
  `content_articleid` int(11) DEFAULT '0' COMMENT '文章ID',
  `content_type` varchar(50) DEFAULT '' COMMENT '字段类型',
  `content_name` varchar(100) DEFAULT '' COMMENT '字段名称',
  `content_value` text COMMENT '字段内容',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM AUTO_INCREMENT=508 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_site_content
-- ----------------------------
INSERT INTO `site_site_content` VALUES ('440', '1', '45', '24', '93', 'checkbox', '多选框', 'a:2:{i:0;s:7:\"内容3\";i:1;s:7:\"内容4\";}');
INSERT INTO `site_site_content` VALUES ('434', '3', '0', '20', '0', 'text', '联系电话', '0571-88888881');
INSERT INTO `site_site_content` VALUES ('196', '1', '45', '15', '12', 'select', '下拉列表', '轴承参数');
INSERT INTO `site_site_content` VALUES ('197', '1', '45', '16', '12', 'radio', '文章类型', '热点');
INSERT INTO `site_site_content` VALUES ('198', '1', '45', '19', '12', 'editor', '产品参数', '<p>大家复工都自己带饭吗？现在带饭去公司每天都要面临这样的痛苦啊！11点半同事开始热饭，公司200多人至少有40多人自己带饭，2个微波炉，基本排到我的时候都快12点半了...等到饥肠辘辘。打算和其他同事一起点团餐了，这么热饭实在太折腾了！sss</p>');
INSERT INTO `site_site_content` VALUES ('195', '1', '45', '14', '12', 'textarea', '测试内容', '现在带饭去公司每天都要面临这样的痛苦啊！ss');
INSERT INTO `site_site_content` VALUES ('194', '1', '45', '10', '12', 'text', '测试内容', '大家复工都自己带饭吗？ss');
INSERT INTO `site_site_content` VALUES ('394', '1', '45', '15', '5', 'select', '下拉列表', '轴承分类');
INSERT INTO `site_site_content` VALUES ('397', '1', '45', '15', '10', 'select', '下拉列表', '轴承分类');
INSERT INTO `site_site_content` VALUES ('363', '1', '45', '15', '2', 'select', '下拉列表', '轴承分类');
INSERT INTO `site_site_content` VALUES ('344', '1', '45', '15', '41', 'select', '下拉列表', '轴承分类');
INSERT INTO `site_site_content` VALUES ('403', '1', '45', '24', '85', 'checkbox', '多选框', 'a:2:{i:0;s:7:\"内容3\";i:1;s:7:\"内容4\";}');
INSERT INTO `site_site_content` VALUES ('402', '1', '45', '19', '85', 'editor', '产品参数', '<p>abc</p><p>abc</p><p>abc</p><p>abc</p>');
INSERT INTO `site_site_content` VALUES ('401', '1', '45', '16', '85', 'radio', '文章类型', '热点');
INSERT INTO `site_site_content` VALUES ('400', '1', '45', '15', '85', 'select', '下拉列表', '轴承参数');
INSERT INTO `site_site_content` VALUES ('483', '1', '0', '1', '0', 'select', '客服电话', '电话号码');
INSERT INTO `site_site_content` VALUES ('484', '1', '0', '2', '0', 'radio', '备案号码', '浙B');
INSERT INTO `site_site_content` VALUES ('485', '1', '0', '3', '0', 'checkbox', '联系邮箱', 'a:2:{i:0;s:7:\"多选1\";i:1;s:7:\"多选3\";}');
INSERT INTO `site_site_content` VALUES ('486', '1', '0', '4', '0', 'textarea', '详细地址', 'abc');
INSERT INTO `site_site_content` VALUES ('487', '1', '0', '5', '0', 'text', '公司坐标', '369854,12365478');
INSERT INTO `site_site_content` VALUES ('488', '1', '0', '11', '0', 'image', '网站Logo', '/uploads/20200921/5f67ebe5100b5.png');
INSERT INTO `site_site_content` VALUES ('441', '1', '45', '15', '82', 'select', '下拉列表', '轴承分类');
INSERT INTO `site_site_content` VALUES ('294', '1', '56', '21', '80', 'text', '招聘人员', '10人');
INSERT INTO `site_site_content` VALUES ('295', '1', '56', '22', '80', 'select', '学历要求', '大专学历');
INSERT INTO `site_site_content` VALUES ('296', '1', '56', '23', '80', 'text', '年龄要求', '20~25');
INSERT INTO `site_site_content` VALUES ('442', '1', '45', '24', '82', 'checkbox', '多选框', 'a:2:{i:0;s:7:\"内容1\";i:1;s:7:\"内容2\";}');
INSERT INTO `site_site_content` VALUES ('367', '1', '56', '23', '81', 'text', '年龄要求', '20~25');
INSERT INTO `site_site_content` VALUES ('366', '1', '56', '22', '81', 'select', '学历要求', '大专学历');
INSERT INTO `site_site_content` VALUES ('365', '1', '56', '21', '81', 'text', '招聘人员', '20人');
INSERT INTO `site_site_content` VALUES ('398', '1', '45', '10', '85', 'text', '测试内容', '测试内容1');
INSERT INTO `site_site_content` VALUES ('399', '1', '45', '14', '85', 'textarea', '测试内容', '测试内容2');
INSERT INTO `site_site_content` VALUES ('395', '1', '45', '15', '7', 'select', '下拉列表', '轴承分类');
INSERT INTO `site_site_content` VALUES ('396', '1', '45', '15', '3', 'select', '下拉列表', '轴承分类');
INSERT INTO `site_site_content` VALUES ('439', '1', '45', '19', '93', 'editor', '产品参数', '<p>abc</p><p>abc</p><p>abc</p><p>abc</p>');
INSERT INTO `site_site_content` VALUES ('437', '1', '45', '15', '93', 'select', '下拉列表', '轴承参数');
INSERT INTO `site_site_content` VALUES ('438', '1', '45', '16', '93', 'radio', '文章类型', '热点');
INSERT INTO `site_site_content` VALUES ('435', '1', '45', '10', '93', 'text', '测试内容', '测试内容1');
INSERT INTO `site_site_content` VALUES ('436', '1', '45', '14', '93', 'textarea', '测试内容', '测试内容2');
INSERT INTO `site_site_content` VALUES ('444', '1', '45', '15', '94', 'select', '下拉列表', '轴承分类');
INSERT INTO `site_site_content` VALUES ('445', '1', '45', '24', '94', 'checkbox', '多选框', 'a:4:{i:0;s:7:\"内容1\";i:1;s:7:\"内容2\";i:2;s:7:\"内容3\";i:3;s:7:\"内容4\";}');
INSERT INTO `site_site_content` VALUES ('490', '1', '0', '25', '0', 'text', '版本号', '1.0');
INSERT INTO `site_site_content` VALUES ('489', '1', '0', '12', '0', 'file', '附件内容', '');
INSERT INTO `site_site_content` VALUES ('505', '1', '54', '33', '65', 'text', '测试内容', 'abc');
INSERT INTO `site_site_content` VALUES ('506', '1', '54', '34', '65', 'text', '测试内容2', 'def');
INSERT INTO `site_site_content` VALUES ('507', '1', '54', '35', '65', 'checkbox', '测试内容3', 'a:1:{i:0;s:3:\"ccc\";}');

-- ----------------------------
-- Table structure for `site_site_param`
-- ----------------------------
DROP TABLE IF EXISTS `site_site_param`;
CREATE TABLE `site_site_param` (
  `param_id` int(11) NOT NULL AUTO_INCREMENT,
  `param_siteid` int(11) DEFAULT '0',
  `param_columnid` int(11) DEFAULT '0',
  `param_name` varchar(50) DEFAULT '' COMMENT '参数名称',
  `param_tips` varchar(255) DEFAULT '' COMMENT '提示',
  `param_type` varchar(50) DEFAULT '' COMMENT '类型/input/select/checkbox/editor',
  `param_value` text COMMENT '参数值',
  `param_sort` int(11) DEFAULT '0' COMMENT '排序号',
  `param_required` tinyint(4) DEFAULT '0' COMMENT '是否必填',
  `param_create_time` int(11) DEFAULT '0',
  `param_update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`param_id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_site_param
-- ----------------------------
INSERT INTO `site_site_param` VALUES ('1', '1', '0', '客服电话', '座机必须加区号', 'select', '\n测试内容\n电话号码\n东西测试仪\n\n', '0', '1', '1582770509', '1612789435');
INSERT INTO `site_site_param` VALUES ('2', '1', '0', '备案号码', '备案号码', 'radio', '浙A\n浙B', '0', '0', '1582778365', '1583741073');
INSERT INTO `site_site_param` VALUES ('3', '1', '0', '联系邮箱', '', 'checkbox', '多选1\n多选2\n多选3', '0', '0', '1582778422', '1583741078');
INSERT INTO `site_site_param` VALUES ('4', '1', '0', '详细地址', '详细地址', 'textarea', '', '0', '1', '1582778455', '1612789451');
INSERT INTO `site_site_param` VALUES ('5', '1', '0', '公司坐标', '坐标地址', 'text', '', '0', '0', '1582778484', '1612789405');
INSERT INTO `site_site_param` VALUES ('6', '1', '12', '电话号码', '区号必须填写', 'text', '', '0', '0', '1582781205', '1582781292');
INSERT INTO `site_site_param` VALUES ('10', '1', '45', '测试内容', '内容来源', 'text', '', '0', '0', '1583678198', '1584164809');
INSERT INTO `site_site_param` VALUES ('11', '1', '0', '网站Logo', '', 'image', '', '0', '0', '1583713716', '1583752121');
INSERT INTO `site_site_param` VALUES ('12', '1', '0', '附件内容', '', 'file', '', '0', '0', '1583751976', '1583751976');
INSERT INTO `site_site_param` VALUES ('14', '1', '45', '测试内容', '', 'textarea', '', '0', '0', '1584164825', '1584164825');
INSERT INTO `site_site_param` VALUES ('15', '1', '45', '下拉列表', '', 'select', '轴承分类\n轴承参数\n轴承内容', '0', '0', '1584164853', '1584164853');
INSERT INTO `site_site_param` VALUES ('16', '1', '45', '文章类型', '', 'radio', '热点\n直播\n科技', '0', '0', '1584164898', '1584164898');
INSERT INTO `site_site_param` VALUES ('17', '1', '45', '测试图片', '', 'image', '', '0', '0', '1584164927', '1584164927');
INSERT INTO `site_site_param` VALUES ('18', '1', '45', '产品PDF', '', 'file', '', '0', '0', '1584164940', '1584164940');
INSERT INTO `site_site_param` VALUES ('19', '1', '45', '产品参数', '', 'editor', '', '0', '0', '1584164950', '1584164950');
INSERT INTO `site_site_param` VALUES ('20', '3', '0', '联系电话', '0571', 'text', '', '0', '0', '1584167289', '1584167289');
INSERT INTO `site_site_param` VALUES ('21', '1', '56', '招聘人员', '', 'text', '', '0', '0', '1584703860', '1584703860');
INSERT INTO `site_site_param` VALUES ('22', '1', '56', '学历要求', '', 'select', '大专学历\n本科学历\n研究生学历\n博士学历', '0', '1', '1584703901', '1584704009');
INSERT INTO `site_site_param` VALUES ('23', '1', '56', '年龄要求', '25岁内', 'text', '', '0', '0', '1584703935', '1584703935');
INSERT INTO `site_site_param` VALUES ('24', '1', '45', '多选框', '', 'checkbox', '内容1\n内容2\n内容3\n内容4', '0', '0', '1589039597', '1589039597');
INSERT INTO `site_site_param` VALUES ('25', '1', '0', '版本号', '样式文件版本号', 'text', '', '0', '0', '1607556861', '1607556861');
INSERT INTO `site_site_param` VALUES ('33', '1', '54', '测试内容', '', 'text', '', '0', '0', '1623678018', '1623678018');
INSERT INTO `site_site_param` VALUES ('34', '1', '54', '测试内容2', '', 'text', '', '0', '0', '1623678027', '1623678027');
INSERT INTO `site_site_param` VALUES ('35', '1', '54', '测试内容3', '', 'checkbox', 'aaa\nbbb\nccc', '0', '0', '1623678048', '1623678048');

-- ----------------------------
-- Table structure for `site_wsarticle`
-- ----------------------------
DROP TABLE IF EXISTS `site_wsarticle`;
CREATE TABLE `site_wsarticle` (
  `wsarticle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wsarticle_siteid` int(11) DEFAULT '0',
  `wsarticle_columnid` int(11) DEFAULT '0',
  `wsarticle_pid` bigint(20) DEFAULT '0' COMMENT '父文章ID',
  `wsarticle_title` varchar(255) DEFAULT '' COMMENT '文章标题',
  `wsarticle_page` varchar(255) DEFAULT '' COMMENT 'url名称',
  `wsarticle_desc` text COMMENT '文章介绍',
  `wsarticle_cateid` varchar(255) DEFAULT '' COMMENT '文章分类',
  `wsarticle_keywords` varchar(255) DEFAULT '' COMMENT '文章关键词',
  `wsarticle_description` varchar(255) DEFAULT '' COMMENT '文章描述',
  `wsarticle_details` longtext COMMENT '文章内容',
  `wsarticle_image` text COMMENT '图片',
  `wsarticle_file` text COMMENT '文件',
  `wsarticle_sort` int(11) DEFAULT '0' COMMENT '文章排序',
  `wsarticle_home` int(11) DEFAULT '0' COMMENT '首页显示',
  `wsarticle_recommend` int(11) DEFAULT '0' COMMENT '推荐',
  `wsarticle_views` int(11) DEFAULT '0' COMMENT '浏览量',
  `wsarticle_author` varchar(255) DEFAULT '' COMMENT '作者',
  `wsarticle_source` varchar(255) DEFAULT '' COMMENT '来源',
  `wsarticle_create_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `wsarticle_update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wsarticle_id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_wsarticle
-- ----------------------------
INSERT INTO `site_wsarticle` VALUES ('1', '1', '45', '0', 'Hello World', 'hello-world', 'Hello World', '107', '', '', '<p>Hello World</p>', 'a:1:{i:1;a:4:{s:9:\"file_name\";s:9:\"logo2.png\";s:9:\"file_path\";s:35:\"/uploads/20200314/5e6bb955e5aab.png\";s:8:\"file_ext\";s:3:\"png\";s:9:\"file_size\";s:7:\"30.41KB\";}}', null, '0', '0', '0', '367', 'admin', '今日头条', '1584114058', '1584155473');
INSERT INTO `site_wsarticle` VALUES ('2', '1', '45', '0', '新冠病毒可能是美军带到武汉？', 'xgbdknsmjddwh', '【环球时报-环球网报道】2020年3月13日，外交部发言人耿爽主持召开例行记者会，部分内容如下。\n在13日举行的外交部记者会上，有外媒记者问及，最近有言论认为新型冠状病毒可能是美军带到武汉。对此，中国外交部发言人...', '0,109,107', '', '【环球时报-环球网报道】2020年3月13日，外交部发言人耿爽主持召开例行记者会，部分内容如下。\n在13日举行的外交部记者会上，有外媒记者问及，最近有言论认为新型冠状病毒可能是美军带到武汉。对此，中国外交部发言人...', '<p>【环球时报-环球网报道】2020年3月13日，外交部发言人耿爽主持召开例行记者会，部分内容如下。</p>\n<p>在13日举行的外交部记者会上，有外媒记者问及，最近有言论认为新型冠状病毒可能是美军带到武汉。对此，中国外交部发言人耿爽回应称，中方注意到最近一段时间有一些关于新冠病毒源头的讨论，个别美国政府高官和国会议员借此发表种种不负责任和不实言论，抹黑攻击中国，对此中方坚决反对。</p>\n<p>耿爽表示，国际社会对病毒源头问题有不同看法。中方始终认为，这是一个科学问题，需要听取科学、专业的意见。</p>', 'a:0:{}', null, '0', '0', '0', '1', 'admin', '', '1584114078', '1596175016');
INSERT INTO `site_wsarticle` VALUES ('3', '1', '45', '0', '意大利汶川时留下一座黑科技医院，12年后，四川人说这情我们还了', 'yi-da-li-wen-chuan-shi-liu-xia', '汶川地震后第六天，成都双流机场上空的阴云里，俯冲下来两架专机。\n机身尾翼上涂着意大利的国旗。\n第一架专机中，塞满总计240顶野战帐篷，总重43吨。\n这种野战帐篷规格大质量高，足以应对暴风雪等极端天气，造价也高...', '108,125,109', '', '汶川地震后第六天，成都双流机场上空的阴云里，俯冲下来两架专机。\n机身尾翼上涂着意大利的国旗。\n第一架专机中，塞满总计240顶野战帐篷，总重43吨。\n这种野战帐篷规格大质量高，足以应对暴风雪等极端天气，造价也高...', '<p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">汶川地震后第六天，成都双流机场上空的阴云里，俯冲下来两架专机。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">机身尾翼上涂着意大利的国旗。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">第一架专机中，塞满总计240顶野战帐篷，总重43吨。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">这种野战帐篷规格大质量高，足以应对暴风雪等极端天气，造价也高，一顶便达4200欧元。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">第二架专机中，除了60顶野战帐篷，还搭载了5000条毛毯，3.4吨的医疗器械和3.76吨高能饼干。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">为了防止帐篷不会安装，意大利还送来了五名专业技术人员。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">第一架飞机的帐篷，当天就送往了绵阳和德阳，后来成为当地的临时学校，深受孩子们欢迎。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">第二架飞机的物资，被送往阿坝州。那些高能饼干，成为许多村庄的急救粮。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">两架飞机的物资，全部是震后灾区最迫切需要的。</p><p style=\"-webkit-tap-highlight-color: transparent; box-sizing: border-box; margin: 16px 0px; padding: 0px; color: #222222; font-family: &#39;PingFang SC&#39;, &#39;Hiragino Sans GB&#39;, &#39;Microsoft YaHei&#39;, &#39;WenQuanYi Micro Hei&#39;, &#39;Helvetica Neue&#39;, Arial, sans-serif; font-size: 16px; background-color: #ffffff;\">事实上，地震当晚，意大利总统纳波利塔诺就表示：</p>', 'a:0:{}', null, '0', '1', '0', '2', 'admin', '', '1584115332', '1607500728');
INSERT INTO `site_wsarticle` VALUES ('4', '1', '45', '0', '给我逆行勇气的，正是当年的你', 'gwnxyqdzsdndn', '这是来自武汉的声音日记。今天我们为大家讲述这样一对母女的故事。妈妈叫韩金香，是北京市大兴区人民医院手术室护士长，女儿叫李佳辰，她是北京大学第一医院重症医学科的医务人员，目前在武汉战“疫”一线。', '107', '给我逆行勇气的，正是当年的你', '给我逆行勇气的，正是当年的你', '<p>这是来自武汉的声音日记。今天我们为大家讲述这样一对母女的故事。妈妈叫韩金香，是北京市大兴区人民医院手术室护士长，女儿叫李佳辰，她是北京大学第一医院重症医学科的医务人员，目前在武汉战&ldquo;疫&rdquo;一线。</p>', null, null, '0', '0', '0', '501', 'admin', '今日头条', '1584116660', '1584118308');
INSERT INTO `site_wsarticle` VALUES ('5', '1', '45', '0', '中国关闭珠穆朗玛峰通道', 'zhong-guo-guan-bi-zhu-mu-lang-ma-feng-tong-dao', '中国关闭珠穆朗玛峰通道 】3月11日，中国西藏登山协会宣布，关闭珠峰北坡（中国西藏一侧）的通道，防止珠穆朗玛峰大本营爆发新冠病毒，且治疗和救援难度大。不允许任何探险队在山峰北侧攀登，开春后的一系列登山活动...', '108,125', '', '中国关闭珠穆朗玛峰通道 】3月11日，中国西藏登山协会宣布，关闭珠峰北坡（中国西藏一侧）的通道，防止珠穆朗玛峰大本营爆发新冠病毒，且治疗和救援难度大。不允许任何探险队在山峰北侧攀登，开春后的一系列登山活动...', '<p><span style=\"color: #505050; font-family: helvetica; font-size: 18px; text-align: justify; background-color: #ffffff;\">中国关闭珠穆朗玛峰通道 】3月11日，中国西藏登山协会宣布，关闭珠峰北坡（中国西藏一侧）的通道，防止珠穆朗玛峰大本营爆发新冠病毒，且治疗和救援难度大。不允许任何探险队在山峰北侧攀登，开春后的一系列登山活动被取消。&nbsp;</span></p><p style=\"text-align: center;\"><span style=\"color: #505050; font-family: helvetica; font-size: 18px; text-align: justify; background-color: #ffffff;\"><img src=\"/uploads/tinymce/20200314/5e6bb4600dd32.jpg\" alt=\"\" width=\"440\" height=\"243\"/></span></p>', 'a:1:{i:101;a:4:{s:9:\"file_name\";s:23:\"8789695741008497420.jpg\";s:9:\"file_path\";s:35:\"/uploads/20200314/5e6bb47040394.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"37.64KB\";}}', null, '0', '1', '0', '501', 'admin', '', '1584116792', '1607500190');
INSERT INTO `site_wsarticle` VALUES ('6', '1', '45', '0', '教育部最新通知来了', 'jiao-yu-bu-zui-xin-tong-zhi-la', '', '125', '', '', '<p>近日，教育部发布《关于公布2019年度普通高等学校本科专业备案和审批结果的通知》。</p>\n<p>《通知》称，本年度各高校新增备案专业1672个、审批专业181个（含130个国家控制布点专业和51个目录外新专业），调整学位授予门类或修业年限专业47个，撤销专业367个。</p>\n<p style=\"text-align: center;\"><img src=\"http://p3.pstatp.com/large/pgc-image/RsEpSmM69FAdzh\" alt=\"教育部最新通知来了\" width=\"100%\" /></p>\n<p>撤销最多的专业</p>\n<p>各高校撤销的专业中，服装与服饰设计、公共事业管理、信息管理与信息系统、市场营销、经济统计学等专业排位靠前。</p>\n<p style=\"text-align: center;\"><img src=\"http://p1.pstatp.com/large/pgc-image/RsIDKbUDAzaVAA\" alt=\"教育部最新通知来了\" /></p>\n<p>&nbsp;</p>', null, null, '0', '0', '0', '1', 'admin', '', '1584156656', '1584156740');
INSERT INTO `site_wsarticle` VALUES ('7', '1', '45', '0', '新冠病毒可能是美军带到武汉？', 'xin-guan-bing-du-ke-neng-shi-mei-jun-dai-dao-wu-ha', '新冠病毒可能是美军带到武汉？', '108,125,151', '', '新冠病毒可能是美军带到武汉？', '<p>新冠病毒可能是美军带到武汉？</p>', 'a:1:{i:101;a:4:{s:9:\"file_name\";s:12:\"banner02.jpg\";s:9:\"file_path\";s:35:\"/uploads/20200314/5e6ce933202c2.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"323.6KB\";}}', null, '0', '0', '0', '7', 'admin', '', '1584195761', '1607500202');
INSERT INTO `site_wsarticle` VALUES ('51', '1', '51', '0', 'banner', '/', 'UPS today announced a series of service \nUPS today announced a series of service ', '0', '', '', 'UPS today announced a series of service \nUPS today announced a series of service ', 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200623/5ef1aabf0cd6e.jpg\";s:9:\"file_name\";s:6:\"banner\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', null, '10', '0', '0', '0', '', '', '1584438982', '1623676332');
INSERT INTO `site_wsarticle` VALUES ('53', '1', '53', '0', '新冠病毒可能是美军带到武汉？', 'xin-guan-bing-du-ke-neng-shi-mei', '', '129,134', '', '', '', 'a:1:{i:101;a:4:{s:9:\"file_name\";s:9:\"logo2.png\";s:9:\"file_path\";s:35:\"/uploads/20200319/5e736ec90692b.png\";s:8:\"file_ext\";s:3:\"png\";s:9:\"file_size\";s:7:\"30.41KB\";}}', null, '0', '1', '1', '1', 'admin', '', '1584623299', '1607514926');
INSERT INTO `site_wsarticle` VALUES ('41', '1', '45', '0', '新冠病毒可能是美军带到武汉？', 'xin-guan-bing-du-ke-neng-shi-mei', '新冠病毒可能是美军带到武汉？', '0,108', '', '新冠病毒可能是美军带到武汉？', '<p>新冠病毒可能是美军带到武汉？</p>', 'a:0:{}', null, '0', '0', '0', '14', 'admin', '', '1584409949', '1592753698');
INSERT INTO `site_wsarticle` VALUES ('40', '1', '52', '0', '百度', 'http://www.baidu.com', '', '0', '', '', null, 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200316/5e6f85801eb5a.jpg\";s:9:\"file_name\";s:6:\"banner\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', null, '0', '0', '0', '0', '', '', '1584366977', '0');
INSERT INTO `site_wsarticle` VALUES ('88', '1', '51', '0', 'banner', '/', 'UPS today announced a series of service\nUPS today announced a series of service\nUPS today announced a series of service\n', '', '', '', 'UPS today announced a series of service \nUPS today announced a series of service ', 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200717/5f110ac7a2a8e.jpg\";s:9:\"file_name\";s:6:\"banner\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', null, '0', '0', '0', '0', '', '', '1594952395', '1622994613');
INSERT INTO `site_wsarticle` VALUES ('77', '1', '55', '0', '技术文档怎么写 - 简书', '', '技术文档怎么写 - 简书', '136', '', '', null, 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200320/5e7424a98ecfe.jpg\";s:9:\"file_name\";s:5:\"image\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200320/5e7424acb2b1d.pdf\";s:9:\"file_name\";s:4:\"file\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"txt\";}}', '0', '0', '0', '0', '', '', '1584669869', '1584669880');
INSERT INTO `site_wsarticle` VALUES ('10', '1', '45', '0', '中国五千年跳不出的5大历史定律，更是人生定律！', 'zhong-guo-wu-qian-nian-tiao-bu-chu', '中国五千年跳不出的5大历史定律，更是人生定律！', '108,107', '', '中国五千年跳不出的5大历史定律，更是人生定律！', '<p>中国五千年跳不出的5大历史定律，更是人生定律！</p>', 'a:0:{}', null, '0', '0', '0', '25', 'admin', '', '1584200609', '1607500748');
INSERT INTO `site_wsarticle` VALUES ('12', '1', '45', '0', '疫情期间复工后，午餐怎么解决？', 'yi-qing-qi-jian-fu-gong-hou-wu-can-zen-me-jie-jue', 'sss', '109', 's', 's', '<p>疫情期间复工后，午餐怎么解决？</p>\n<p>很多人为此都加入了&ldquo;带饭族&rdquo;</p>\n<p>不过最近有网友发帖说</p>\n<p>自带便当后也遇上了世纪难题&hellip;&hellip;</p>\n<p>19楼网友@别惹我狒狒</p>\n<p>大家复工都自己带饭吗？现在带饭去公司每天都要面临这样的痛苦啊！11点半同事开始热饭，公司200多人至少有40多人自己带饭，2个微波炉，基本排到我的时候都快12点半了...等到饥肠辘辘。打算和其他同事一起点团餐了，这么热饭实在太折腾了！</p>\n<p>以前这个冰箱都没几个盒子的，这两天多了很多。</p>', null, null, '0', '0', '0', '25', 'admin', '', '1584201324', '1584318230');
INSERT INTO `site_wsarticle` VALUES ('47', '1', '53', '0', '国际社会对病毒源头问题有不同看法', 'guo-ji-she-hui-dui-bing-du-yuan', '', '134', '', '', '', 'a:1:{i:1;a:4:{s:9:\"file_name\";s:9:\"code1.jpg\";s:9:\"file_path\";s:35:\"/uploads/20200317/5e7052bf2d299.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"63.25KB\";}}', null, '0', '1', '0', '0', 'admin', '', '1584419355', '0');
INSERT INTO `site_wsarticle` VALUES ('80', '1', '56', '0', 'ASP程序员', 'php-cheng-xu-yuan', 'ASP程序员', '137', '', '', '<h3>岗位职责：</h3>\n<p>1、 负责平台运营的业务支撑工作，保证平台业务稳定发展；</p>\n<p>2、 参与和优化部门业务操作流程，保证团队协同工作；</p>\n<p>3、 为用户提供平台业务咨询服务；</p>\n<p>4、 受理客户投诉，在授权范围内予以解决；</p>\n<p>5、 网络活动视频录像与剪辑，挖掘优秀作品,后台信息简单编辑处理；</p>\n<p>6、 与公司其他部门配合工作。</p>\n<h3><strong>任职要求：</strong></h3>\n<p>1、 专科及以上学历，热爱互联网行业；</p>\n<p>2、 较强的工作责任心，踏实勤恳，积极向上，性格开朗；</p>\n<p>3、 形象佳，口齿伶俐，普通话标准；</p>\n<p>4、 熟练使用电脑，经常上网，会使用office等相关办公软件；</p>\n<p>5、 能适应白班、夜班倒班工作制；</p>\n<p>注：根据个人能力和特长，公司给予更多的发展及晋升空间。</p>', null, null, '0', '0', '1', '1', 'admin', '', '1584703966', '1584879443');
INSERT INTO `site_wsarticle` VALUES ('79', '1', '55', '0', 'EPE制品', 'epe-zhi-pin', 'EPE制品', '136', '', '', null, 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200320/5e74325bd06e1.jpg\";s:9:\"file_name\";s:5:\"image\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200320/5e74325ed11b2.pdf\";s:9:\"file_name\";s:4:\"file\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"txt\";}}', '0', '0', '0', '16', 'Yimao软件', '', '1584673376', '1596196998');
INSERT INTO `site_wsarticle` VALUES ('63', '1', '54', '0', '百度', '', '', '135', '', '', null, 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200320/5e740ac2b5ef6.jpg\";s:9:\"file_name\";s:5:\"image\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', null, '0', '0', '0', '0', '', '', '1584663236', '0');
INSERT INTO `site_wsarticle` VALUES ('65', '1', '54', '0', 'aardio  12', '', '产品图片内容', '135', '', '', null, 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200320/5e740c7b5cd68.png\";s:9:\"file_name\";s:6:\"banner\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', null, '0', '0', '0', '0', '', '', '1584663676', '1623678434');
INSERT INTO `site_wsarticle` VALUES ('68', '1', '52', '0', 'banner', 'http://www.baidu.com', '', '0', '', '', null, 'a:0:{}', null, '0', '0', '0', '0', '', '', '1584665796', '1592740399');
INSERT INTO `site_wsarticle` VALUES ('81', '1', '56', '0', 'PHP程序员', 'php-cheng-xu-yuan', 'PHP程序员', '138', '', '岗位职责123：1、 负责平台运营的业务支撑工作，保证平台业务稳定发展；2、 参与和优化部门业务操作流程，保证团队协同工作；3、 为用户提供平台业务咨询服务；4、 受理客户投诉，在授权范围内予以解决；5、 网络活动...', '<h3>岗位职责123：</h3><p>1、 负责平台运营的业务支撑工作，保证平台业务稳定发展；</p><p>2、 参与和优化部门业务操作流程，保证团队协同工作；</p><p>3、 为用户提供平台业务咨询服务；</p><p>4、 受理客户投诉，在授权范围内予以解决；</p><p>5、 网络活动视频录像与剪辑，挖掘优秀作品,后台信息简单编辑处理；</p><p>6、 与公司其他部门配合工作。</p><h3><strong>任职要求123：</strong></h3><p>1、 专科及以上学历，热爱互联网行业；</p><p>2、 较强的工作责任心，踏实勤恳，积极向上，性格开朗；</p><p>3、 形象佳，口齿伶俐，普通话标准；</p><p>4、 熟练使用电脑，经常上网，会使用office等相关办公软件；</p><p>5、 能适应白班、夜班倒班工作制；</p><p>注：根据个人能力和特长，公司给予更多的发展及晋升空间。</p>', 'a:0:{}', 'a:0:{}', '0', '0', '1', '3', 'admin', '', '1584703966', '1596187642');
INSERT INTO `site_wsarticle` VALUES ('82', '1', '45', '0', 'abcabc', 'abcabc', 'abcabc', '108', '', 'abcabc', '<p>abcabc</p>', 'a:0:{}', null, '0', '1', '1', '12', '管理员', '', '1585041623', '1612787614');
INSERT INTO `site_wsarticle` VALUES ('84', '1', '58', '0', 'PHP开发文档', 'php-kai-fa-wen-dang', 'PHP开发文档', '147', '', '', '<p>PHP开发文档</p>', 'a:0:{}', null, '0', '0', '0', '0', 'Yimao软件', '', '1589162559', '1589180535');
INSERT INTO `site_wsarticle` VALUES ('85', '1', '45', '0', '端午节快到了，来尝试一下怎么包粽子', 'duan-wu-jie-kuai-dao-le-lai-chang', '大家好，我是云霞，端午节将至，我从老家摘了一些新鲜粽叶带回来，打算尝试自己包棕子。\n这次所有流程都是我第一次尝试，之前在老家看过我婆婆怎么包，自己亲手包粽子，不仅吃起来格外美味，而且很有成就感。\n我打算...', '108,109', '', '大家好，我是云霞，端午节将至，我从老家摘了一些新鲜粽叶带回来，打算尝试自己包棕子。\n这次所有流程都是我第一次尝试，之前在老家看过我婆婆怎么包，自己亲手包粽子，不仅吃起来格外美味，而且很有成就感。\n我打算...', '<p>大家好，我是云霞，端午节将至，我从老家摘了一些新鲜粽叶带回来，打算尝试自己包棕子。</p><p>这次所有流程都是我第一次尝试，之前在老家看过我婆婆怎么包，自己亲手包粽子，不仅吃起来格外美味，而且很有成就感。</p><p>我打算包两种口味的棕子：红豆棕和蛋黄肉棕。在这里分享给大家。</p><p>步骤1：首先蜜豆清洗干净泡一晚上，糯米提前两个小时加水浸泡</p><p><img src=\"http://p1.pstatp.com/large/pgc-image/4108e0b071094b9b9bbc806e694325a8\" alt=\"端午节快到了，来尝试一下怎么包粽子\"/></p><p>步骤2：新鲜棕叶用开水泡一下再冲洗干净在水里泡一会，如果是干粽叶要提前一晚泡软，再放入开水锅内烫软，捞出后刷洗干净浸泡在清水中。</p><p><img src=\"http://p3.pstatp.com/large/pgc-image/5ed70f406d8a41679729520d82556928\" alt=\"端午节快到了，来尝试一下怎么包粽子\"/></p><p>步骤3：猪肉切块或切条放入生抽，老抽，蚝油，胡椒粉，少许盐和糖拌匀，再放入冰箱腌制2-4小时。</p><p><img src=\"http://p9.pstatp.com/large/pgc-image/887342df5488485e839fd0394fcf966d\" alt=\"端午节快到了，来尝试一下怎么包粽子\"/></p><p>步骤4：准备咸鸭蛋黄（我这个是直接淘宝上买的包装蛋黄），如果有板栗的话可以准备点剥好的板栗</p>', 'a:0:{}', null, '0', '1', '1', '23', 'Yimao软件', '', '1592583845', '1607514555');
INSERT INTO `site_wsarticle` VALUES ('87', '1', '53', '0', 'ab ab abc', 'ab-ab-abc', 'ab ab abc', '134,129,131', '', 'ab ab abc ab ab abc', '<p>ab&nbsp;</p><p>ab&nbsp;</p><p>abc&nbsp;</p><p>ab ab abc</p><p><br/></p>', 'a:1:{i:101;a:4:{s:9:\"file_name\";s:13:\"tree card.jpg\";s:9:\"file_path\";s:35:\"/uploads/20200625/5ef4aa7d4bf71.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"67.24KB\";}}', 'a:0:{}', '0', '0', '0', '18', '管理员', '', '1585041647', '1608189597');
INSERT INTO `site_wsarticle` VALUES ('93', '1', '45', '0', '端午节快到了，来尝试一下怎么包粽子', 'duan-wu-jie-kuai-dao-le-lai-chang', '大家好，我是云霞，端午节将至，我从老家摘了一些新鲜粽叶带回来，打算尝试自己包棕子。\n这次所有流程都是我第一次尝试，之前在老家看过我婆婆怎么包，自己亲手包粽子，不仅吃起来格外美味，而且很有成就感。\n我打算...', '108,109,152', '', '大家好，我是云霞，端午节将至，我从老家摘了一些新鲜粽叶带回来，打算尝试自己包棕子。\n这次所有流程都是我第一次尝试，之前在老家看过我婆婆怎么包，自己亲手包粽子，不仅吃起来格外美味，而且很有成就感。\n我打算...', '<p>大家好，我是云霞，端午节将至，我从老家摘了一些新鲜粽叶带回来，打算尝试自己包棕子。</p><p>这次所有流程都是我第一次尝试，之前在老家看过我婆婆怎么包，自己亲手包粽子，不仅吃起来格外美味，而且很有成就感。</p><p>我打算包两种口味的棕子：红豆棕和蛋黄肉棕。在这里分享给大家。</p><p>步骤1：首先蜜豆清洗干净泡一晚上，糯米提前两个小时加水浸泡</p><p><img src=\"http://p1.pstatp.com/large/pgc-image/4108e0b071094b9b9bbc806e694325a8\" alt=\"端午节快到了，来尝试一下怎么包粽子\"/></p><p>步骤2：新鲜棕叶用开水泡一下再冲洗干净在水里泡一会，如果是干粽叶要提前一晚泡软，再放入开水锅内烫软，捞出后刷洗干净浸泡在清水中。</p><p><img src=\"http://p3.pstatp.com/large/pgc-image/5ed70f406d8a41679729520d82556928\" alt=\"端午节快到了，来尝试一下怎么包粽子\"/></p><p>步骤3：猪肉切块或切条放入生抽，老抽，蚝油，胡椒粉，少许盐和糖拌匀，再放入冰箱腌制2-4小时。</p><p><img src=\"http://p9.pstatp.com/large/pgc-image/887342df5488485e839fd0394fcf966d\" alt=\"端午节快到了，来尝试一下怎么包粽子\"/></p><p>步骤4：准备咸鸭蛋黄（我这个是直接淘宝上买的包装蛋黄），如果有板栗的话可以准备点剥好的板栗</p>', 'a:1:{i:101;a:4:{s:9:\"file_name\";s:10:\"about1.jpg\";s:9:\"file_path\";s:35:\"/uploads/20201217/5fdb14bfd1f6c.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:8:\"125.15KB\";}}', 'a:0:{}', '0', '1', '1', '32', 'Yimao软件', '', '1592583845', '1608945842');
INSERT INTO `site_wsarticle` VALUES ('89', '1', '51', '0', 'banner', '/', 'UPS today announced a series of service\nUPS today announced a series of service\nUPS today announced a series of service\n', '', '', '', 'UPS today announced a series of service \nUPS today announced a series of service ', 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200717/5f110aea63377.jpg\";s:9:\"file_name\";s:6:\"banner\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', 'a:0:{}', '0', '0', '0', '0', 'Yimao软件', '', '1594952395', '1622994607');
INSERT INTO `site_wsarticle` VALUES ('90', '1', '51', '0', 'banner', '/', 'UPS today announced a series of service \nUPS today announced a series of service ', '0', '', '', 'UPS today announced a series of service \nUPS today announced a series of service ', 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200623/5f211e1c8822f.jpg\";s:9:\"file_name\";s:6:\"banner\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', 'a:0:{}', '10', '0', '0', '0', 'Yimao软件', '', '1584438982', '1623676103');
INSERT INTO `site_wsarticle` VALUES ('91', '1', '55', '0', 'EPE制品', 'epe-zhi-pin', 'EPE制品', '136', '', '', null, 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200320/5f2408f894dcd.jpg\";s:9:\"file_name\";s:5:\"image\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20200320/5f2408f895b7f.pdf\";s:9:\"file_name\";s:4:\"file\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"txt\";}}', '0', '0', '0', '1', 'Yimao软件', '', '1584673376', '1596196998');
INSERT INTO `site_wsarticle` VALUES ('92', '1', '55', '0', 'PHS Club Meeting', 'phs-club-meeting', 'PHS Club Meeting', '136', '', '', null, 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20201209/5fd0ad133e24e.png\";s:9:\"file_name\";s:5:\"image\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"jpg\";}}', 'a:1:{i:0;a:4:{s:9:\"file_path\";s:35:\"/uploads/20201209/5fd0ad1d29cd3.zip\";s:9:\"file_name\";s:4:\"file\";s:9:\"file_size\";s:3:\"0Kb\";s:8:\"file_ext\";s:3:\"txt\";}}', '0', '0', '0', '2', 'Yimao软件', '', '1607511328', '0');
INSERT INTO `site_wsarticle` VALUES ('94', '1', '45', '0', '新冠病毒可能是美军带到武汉？', 'xin-guan-bing-du-ke-neng-shi-mei', '新冠病毒可能是美军带到武汉？', '108,108', '', '新冠病毒可能是美军带到武汉？', '<p>新冠病毒可能是美军带到武汉？</p>', 'a:0:{}', 'a:0:{}', '0', '0', '0', '24', 'admin', '', '1584409949', '1622898277');
INSERT INTO `site_wsarticle` VALUES ('95', '1', '53', '0', '联想笔记本', 'lian-xiang-bi-ji-ben', '首先要掌握书本上基础的字词，可采用读、背、默写的方式：包括生字、解词、近义词、反义词、形近字；句子，包括把字句，被字句，反问句，第二人称转述句，修改病句；阅读理解：包括段落的划分，中心句，修辞手法，根据内容回答问题；分类作文技巧等', '134', '', '首先要掌握书本上基础的字词，可采用读、背、默写的方式：包括生字、解词、近义词、反义词、形近字；句子，包括把字句，被字句，反问句，第二人称转述句，修改病句；阅读理解：包括段落的划分，中心句，修辞手法，根...', '<p>首先要掌握书本上基础的字词，可采用读、背、默写的方式：包括生字、解词、近义词、反义词、形近字；句子，包括把字句，被字句，反问句，第二人称转述句，修改病句；阅读理解：包括段落的划分，中心句，修辞手法，根据内容回答问题；分类作文技巧等。句子和阅读及写作可以买一些资料练习。。那么这方面的复习内容如何确定？我们要结合单元重点来进行。语文教材每个单元都有不同的侧重点，这个侧重点就是本单元的训练重点，教材每单元前的课文导读就给出了单元重点，我们在复习的过程中要以这个单元重点为主线来贯穿整个单元的课文复习，把每篇课文的内容、中心、重点段、文后的重点题目有机的结合起来，给学生一个整体的感知。?二、采用不同的方式方法?</p><p>我们确立复习内容之后，就要考虑方法的问题了。?1、积累方面主要采用读、背、默的方法。?</p><p>这里的“读”可采用齐读、自由读、默读、同桌之间合作读、边读边思、师生合作读等多种方式。?</p><p>“背”主要采用师生合作、学生之间合作等方式，要充分利用小组长。也可以和默写相结合。?</p><p>这里的“默”指默写，默写时教师要结合实际来进行，就默写的内容而言可以有字词、读读写写、读读记记、日积月累、要求背诵的较短的课文等；另外，默写的形式也要注意，可采取全默、分层次默写（如生字的默写，可采用多次，一次过关几人，要特别关注差生）、学生之间互相默写等方式。默写之后的批改，老师也要采取不同的方式来进行。?</p><p>2、阅读方面主要以讲、练的方法为主。?</p><p>阅读方面的复习首先要求教师梳理好本单元的内容，安排好授课顺序，做到心中有数；要采取多种方式巩固学生的知识点，如文章中心的掌握可以采取不同形式的提问、填空、学生合作、竞赛等方式方法。另外，复习课不要出现教师一言堂的情况，要把更多的时间交给学生，充分发挥学生的主体性。学生的训练要突出重点，要结合单元卷子，结合教材等.?</p><p>3、口语交际和习作方面建议采取归类复习。?以强化和指导为主，不要进行过多的写的练习。?三、合理安排时间?</p><p>就上述三方面的内容我们如何安排时间呢？个人认为，积累方面放在早读及家庭作业中进行，譬如今天要复习第一单元的内容，那么前一天要布置本单元的生字、新词及语文园地中要求默写或背诵的内容为作业，第二天抽时间检查，早读时间也要读背这些内容。阅读方面的知识的理解，重点放在上语文课时进行。习作及口语交际采用归类复习的方式，利用语文课时间进行。</p><p><br/></p>', 'a:2:{i:1;a:4:{s:9:\"file_name\";s:6:\"13.jpg\";s:9:\"file_path\";s:35:\"/uploads/20210605/60bb7957bc169.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"31.49KB\";}i:2;a:4:{s:9:\"file_name\";s:6:\"17.jpg\";s:9:\"file_path\";s:35:\"/uploads/20210605/60bb795e10feb.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"35.95KB\";}}', null, '0', '0', '0', '1', 'Yimao软件', '', '1622898886', '0');
INSERT INTO `site_wsarticle` VALUES ('96', '1', '53', '0', '上海电机学院学历的学员', 'xiang-yao-tong-guo-cheng-ren-gao-kao', '想要通过成人高考考取上海电机学院学历的学员', '130', '', '招生对象：想要通过成人高考考取上海电机学院学历的学员教学目标：帮助学员通过成考，参加上海电机学院成人高等教育1、文凭过硬，为就业、择业、展业打开大门，为执业、提薪、留沪铺平道路。2、在职学习不影响工作，...', '<p>招生对象：想要通过成人高考考取上海电机学院学历的学员</p><p><br/></p><p>教学目标：帮助学员通过成考，参加上海电机学院成人高等教育</p><p><br/></p><p>1、文凭过硬，为就业、择业、展业打开大门，为执业、提薪、留沪铺平道路。</p><p><br/></p><p>2、在职学习不影响工作，学习时间基本为每周一天，学习时间短，拿证书快。</p><p><br/></p><p>3、在校期间通过公共英语三级笔试成绩、全部课程合格即可申请学士学位。</p><p><br/></p><p>4、入学测试一般达到上海市成人高考最低录取分数线即可录取。</p><p><br/></p><p>5、毕业证书教育部电子注册，证书和普通高校毕业证书一致，毕业通过率高。</p><p><br/></p>', 'a:3:{i:101;a:4:{s:9:\"file_name\";s:6:\"21.jpg\";s:9:\"file_path\";s:35:\"/uploads/20210605/60bb79cd9661b.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"10.93KB\";}i:102;a:4:{s:9:\"file_name\";s:6:\"23.jpg\";s:9:\"file_path\";s:35:\"/uploads/20210605/60bb79ce93f71.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"14.88KB\";}i:103;a:4:{s:9:\"file_name\";s:6:\"22.jpg\";s:9:\"file_path\";s:35:\"/uploads/20210605/60bb79cf19232.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"12.93KB\";}}', null, '0', '0', '0', '1', 'Yimao软件', '', '1622899124', '1622899164');
INSERT INTO `site_wsarticle` VALUES ('97', '1', '53', '0', '插花的形式', 'cha-hua-de-xing-shi', '阶梯式·好似一阶一阶的楼梯,每一朵花之间有距离，但不一定等距离,只要有一阶一阶的感觉即可\n\n', '134', '', '插花的形式阶梯式·好似一阶一阶的楼梯,每一朵花之间有距离，但不一定等距离,只要有一阶一阶的感觉即可以点状的花材较适合如：玫瑰、康乃磬、菊花、耳朵火鹤、郁金香·重叠式·只要形状表面是平面的叶或花,都可以用此...', '<p>插花的形式</p><p>阶梯式·好似一阶一阶的楼梯,每一朵花之间有距离，但不一定等距离,只要有一阶一阶的感觉即可</p><p>以点状的花材较适合</p><p>如：玫瑰、康乃磬、菊花、耳朵火鹤、郁金香</p><p>·重叠式·只要形状表面是平面的叶或花,都可以用此技巧表现如：银河叶、本瓜叶、电信兰叶,笑蓉叶、太阳花、向日葵、切片的玉米等都可重叠，每片空隙较小</p><p>·堆积式·堆积的特色是花材多或颜色,有规则的起伏形状好似一波波的海浪,花梗短，花与花之间没有空隙,使用花材则以点状、块状为佳,如：玫瑰、康乃磬、星辰花、孔雀花,甚至石头皆可，适合作底部颜色的表现。</p><p>·焦点·焦点是作品里最明显突出的点,从正面或侧面看都极醒目,以形状特殊、色彩、花朵较大者为佳,如果花朵小则量须多，才能形成焦点如：香水百合、姬百合、孤挺花、天堂鸟/鹤蕉、梦幻蕉、蝴蝶兰等，都可作为焦点花材</p><p>·组群式·此种技巧适用于各种花形,只要将同种类同色系的花材,分组（2枝以上）、分区（一个区可有两种以上不同的组群表现）但每一组之间需有距离</p><p>·平铺式·“平铺”就如字面所示它所表现出来的是“平”的没有任何的高低层次所以它是底部覆盖海绵的技巧之一为求变化，可使用各种不同的花材只要看起来是一个平面即可</p><p>以上就是关于“花艺培训中插花的几种形式介绍”的内容介绍，希望对大家学习有所帮助。想要了解更多关于插花花艺培训的相关资讯欢迎来咨询。</p><p><br/></p>', 'a:3:{i:1;a:4:{s:9:\"file_name\";s:13:\"product-5.jpg\";s:9:\"file_path\";s:35:\"/uploads/20210605/60bb79f2b58bc.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"17.69KB\";}i:2;a:4:{s:9:\"file_name\";s:15:\"product-5-1.jpg\";s:9:\"file_path\";s:35:\"/uploads/20210605/60bb79f3c8dea.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"70.41KB\";}i:3;a:4:{s:9:\"file_name\";s:13:\"product-7.jpg\";s:9:\"file_path\";s:35:\"/uploads/20210605/60bb79f458018.jpg\";s:8:\"file_ext\";s:3:\"jpg\";s:9:\"file_size\";s:7:\"17.87KB\";}}', null, '0', '0', '0', '1', 'Yimao软件', '', '1622899178', '0');
INSERT INTO `site_wsarticle` VALUES ('98', '1', '64', '0', 'PHS Club Meeting', 'phs-club-meeting', 'PHS Club Meeting', '', '', 'PHS Club Meeting', '<p>PHS Club Meeting</p>', null, null, '0', '0', '0', '0', 'Yimao软件', '', '1623331177', '0');

-- ----------------------------
-- Table structure for `site_wscate`
-- ----------------------------
DROP TABLE IF EXISTS `site_wscate`;
CREATE TABLE `site_wscate` (
  `wscate_id` int(11) NOT NULL AUTO_INCREMENT,
  `wscate_siteid` int(11) DEFAULT '0',
  `wscate_columnid` int(11) DEFAULT '0',
  `wscate_pid` int(11) DEFAULT '0',
  `wscate_child` tinyint(4) DEFAULT '0',
  `wscate_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `wscate_sort` int(11) DEFAULT '0',
  `wscate_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '标题',
  `wscate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '名称',
  `wscate_page` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'url名称',
  `wscate_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `wscate_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `wscate_home` smallint(6) DEFAULT '0' COMMENT '推荐',
  `wscate_recommend` smallint(6) DEFAULT '0' COMMENT '推荐',
  `wscate_details` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '内容',
  `wscate_image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '图片',
  PRIMARY KEY (`wscate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_wscate
-- ----------------------------
INSERT INTO `site_wscate` VALUES ('107', '1', '45', '109', '0', '0,109', '3', '行业新闻', '行业新闻', 'hang-ye-xin-wen', '', '', '1', '1', '', 'a:0:{}');
INSERT INTO `site_wscate` VALUES ('108', '1', '45', '0', '0', '0', '1', '公司新闻', '公司新闻', 'gsxw', '', '', '1', '1', '', 'a:0:{}');
INSERT INTO `site_wscate` VALUES ('109', '1', '45', '0', '1', '0', '2', '国际新闻', '国际新闻', 'gjxw', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('138', '1', '56', '0', '0', '0', '2', '北京总公司', '北京总公司', 'bei-jing-zong-gong-si', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('136', '1', '55', '0', '0', '0', '1', '技术文档', '技术文档', 'ji-shu-wen-dang', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('137', '1', '56', '0', '0', '0', '1', '杭州分公司', '杭州分公司', 'hang-zhou-fen-gong-si', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('129', '1', '53', '0', '0', '0', '3', '电脑', '电脑', 'dian-nao', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('130', '1', '53', '0', '0', '0', '2', '台式电脑', '台式电脑', 'tai-shi-dian-nao', '', '', '0', '1', null, null);
INSERT INTO `site_wscate` VALUES ('131', '1', '53', '0', '1', '0', '4', '台式电脑', '台式电脑', 'tai-shi-dian-nao', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('134', '1', '53', '0', '0', '0', '1', '笔记本电脑', '笔记本电脑', 'bi-ji-ben-dian-nao', '', '', '1', '0', '', 'a:0:{}');
INSERT INTO `site_wscate` VALUES ('135', '1', '54', '0', '0', '0', '1', '企业像册', '企业像册', 'qi-ye-xiang-ce', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('152', '1', '45', '125', '0', '0,125', '6', '后端开发人员', '后端开发人员', 'hou-duan-kai-fa-ren-yuan', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('153', '1', '45', '0', '0', '0', '7', '专业新闻', '专业新闻', 'zhuan-ye-xin-wen', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('125', '1', '45', '0', '1', '0', '4', 'PHP程序员', 'PHP程序员', 'cxy', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('148', '1', '53', '131', '0', '0,131', '5', '联想台式机', '联想台式机', 'lian-xiang-tai-shi-ji', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('151', '1', '45', '125', '0', '0,125', '5', '前端开发人员', '前端开发人员', 'qian-duan-kai-fa-ren-yuan', '', '', '0', '0', null, null);
INSERT INTO `site_wscate` VALUES ('147', '1', '58', '0', '0', '0', '1', '技术文档', '技术文档', 'ji-shu-wen-dang', '', '', '0', '0', null, null);

-- ----------------------------
-- Table structure for `site_wsmessage`
-- ----------------------------
DROP TABLE IF EXISTS `site_wsmessage`;
CREATE TABLE `site_wsmessage` (
  `wsmessage_id` int(11) NOT NULL AUTO_INCREMENT,
  `wsmessage_siteid` int(11) DEFAULT '0',
  `wsmessage_columnid` int(11) DEFAULT '0',
  `wsmessage_status` tinyint(4) DEFAULT '0',
  `wsmessage_name` varchar(100) DEFAULT '',
  `wsmessage_phone` varchar(100) DEFAULT NULL,
  `wsmessage_email` varchar(150) DEFAULT NULL,
  `wsmessage_title` varchar(150) DEFAULT NULL,
  `wsmessage_desc` text,
  `wsmessage_ip` varchar(20) DEFAULT '',
  `wsmessage_create_time` int(11) DEFAULT '0',
  `wsmessage_update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`wsmessage_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_wsmessage
-- ----------------------------
INSERT INTO `site_wsmessage` VALUES ('2', '1', '57', '1', '张四', '13588888888', 'zjutsxj@qq.com', null, '我是内容，我是内容，我是内容，我是内容，我是内容，', '192.168.0.12', '1584114058', '0');
INSERT INTO `site_wsmessage` VALUES ('3', '1', '57', '1', '张三', '13588888888', 'zjutsxj@qq.com', null, '我是内容，我是内容，我是内容，我是内容，我是内容，', '192.168.0.12', '1584114058', '0');
INSERT INTO `site_wsmessage` VALUES ('5', '1', '57', '0', 'zjutsxj', '13588254045', 'zjutsxj@qq.com', null, '提交内容', '127.0.0.1', '1622906954', '1622906954');
INSERT INTO `site_wsmessage` VALUES ('6', '1', '57', '0', 'zjutsxj', '13588254045', 'zjutsxj@qq.com', '', 'zjutsxj@qq.com', '127.0.0.1', '1622934235', '1622934235');
