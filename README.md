# Yimao Admin v3.0.5

源码地址：https://gitee.com/zjutsxj/yimaoadmin

演示地址：http://demo.91site.net/

后台地址：http://demo.91site.net/admin

演示帐号：yimao

演示密码：123456

文档地址：https://www.kancloud.cn/zjutsxj/yimaoadmin



#### 介绍
Yimao Admin v3.0.5 企业建站系统，使用 thinkphp5.1.27 + mysql 开发。  
1. php 要求 5.6 以上版本，推荐使用 5.6, 7.0, 7.1，扩展(curl,json,pdo,gd,filter,rewrite)
2. 数据库：mysql 推荐使用5.5/5.6
3. 服务器软件apache或者nginx

#### 系统特点：  
1. 功能模块丰富，功能模块支持无限级分类、支持参数自定义;
2. 添加无限多个语言版本，各个版本之间单独设置互不影响;

#### 使用说明
如果你有 thinkphp5.1.*  的开发经验那你可以轻而易举的使用本系统来制作建企业网站，如果你对 thinkphp 不了解也没关系，只要你会制作 html 页面也可以很方便创建企业网站。 
    

* 将数据库文件 tp51_site.sql 导入到mysql数据库；在 .env 文件中修改数据库连接信息。  
* 主题位置：/public/themes  
* 域名解析：/public 目录 （不是解析到根目录，需要解析到 public 目录）  


##### 站点全局变量（站点内任意位置都可以直接调用）
```
首页标题：{$site_home}
网站名称：{$site_title}
关键词：{$site_keywords}
网站描述：{$site_description}
JS代码：{$site_jscode}
```
以上代码如果需要转义可以这样使用：
```
{$site_jscode|raw}
```

如果定义站点时添加了自定义参数：（查看网站设置页面“其它设置”）
```
例如：{$site_param_1}、{$site_param_2}、{$site_param_3}
```

##### sp_page($id) 单页内容
```
案例 1：{assign name="field" value=":sp_page(10)"}
案例 2：<?php $field = sp_page(10); ?>
```

##### sp_column($id) 栏目内容
```
案例 1：{assign name="field" value=":sp_column(10)"}
案例 2：<?php $field = sp_column(10); ?>
```

##### sp_desc($id) 内容简介
```
案例 1：{:sp_desc(10)}  
案例 2：{:sp_desc(10)|raw}  
案例 3：<?php echo sp_desc(10); ?>
```
#### sp_details($id, $param, $cate) 文章详情
```
案例 1：不带参数，系统自动获取参数变量
{assign name="field" value=":sp_details()"}
<?php $field = sp_details(); ?>

案例 2：指定文章 id
{assign name="field" value=":sp_details(100)"}
<?php $field = sp_details(100); ?>

案例 3：指定文章 id 显示自定义字段
{assign name="field" value=":sp_details(100, true)"}
<?php $field = sp_details(100, true); ?>

案例 4：未指定文章 id 显示自定义字段，显示分类信息
{assign name="field" value=":sp_details(0, true, true)"}
<?php $field = sp_details(0, true, true); ?>

调用自定义字段：{$field['param_36']}  36 为自定义字段的 id 号

调用分类信息
名称：{$field['wscate]['wscate_name']}
标题：{$field['wscate]['wscate_title']}
内容：{$field['wscate]['wscate_details']}
图片：{$field['wscate]['wscate_image']}
```

##### sp_column_cate($cateid) 栏目分类
作用环境，例如：企业动态 =》 企业新闻，行业新闻  
点击 “企业新闻” ，“行业新闻” 页面时使用该函数。
```
变量：$cateid(选填) 未填写时系统将自动获取该 cateid 号   
案例 1：{assign name="field" value=":sp_column_cate(10)"}  
案例 2：{assign name="field" value=":sp_column_cate()"}  
案例 3：<?php $field = sp_column_cate(10); ?>  
案例 4：<?php $field = sp_column_cate(); ?>
```
##### sp_cate($columnid, $parentid, $number, $options) 分类列表
参数说明：
```
$columnid 栏目 id ==(必填)==    
$parentid 父 id ==(选填)==   
$number 获取数量 ==(选填)==  
$options 其它参数 array ==(选填)== ['where'=>'', 'order'=>'']
```

使用方法：
```
案例 1：{assign name="list" value=":sp_cate(10)"}   
说明：获取 columnid=10 所有分类

案例 2：{assign name="list" value=":sp_cate(10, 4)"}
<?php $list = sp_cate(10, 4); ?>  
说明：获取 columnid=10 and parentid=4 所有分类

案例 3：{assign name="list" value=":sp_cate(10, '', 8)"}  
说明：获取 columnid=10 前 8 条分类

案例 4：{assign name="list" value=":sp_cate(10, 0, 6)"}  
说明：获取 columnid=10 and parentid=0 前 6 条分类
```
使用 php 直接调用：
```
案例 1：<?php $list = sp_cate(10); ?>  
案例 2：<?php $list = sp_cate(10, 4); ?>  
案例 3：<?php $list = sp_cate(10, '', 8); ?>   
案例 4：<?php $list = sp_cate(10, 0, 6); ?> 
```

自定义参数 调用
```
<?php   
    $options = [
        // 定义 where 参数后，$columnid, $parentid 这二个参数将会自动失效
        'where' => 'wscate_columnid = 10 and wscate_pid = 5',
        'order' => 'wscate_sort asc' // 排序方式，不填写按默认方式排序
    ];
    $list = sp_cate(0, 0, 10, $options); // 获取 10 条分类
    $list = sp_cate(0, 0, 0, $options); // 获取全部分类
?>
```

##### sp_article($columnid, $cateid, $number, $options) 文章列表
说明：sp_article 函数是 sp_list 的优化版，更多功能请查看 sp_list 函数。  
备注：系统支持分类多选。自定义where语句时，数据分类筛选用 ==find_in_set== 来查询数据。
```
$columnid 栏目 id （必填）  
$cateid 分类 id （选填）
$number 获取数量 （选填）
$options 其它参数 array (选填) where, order, pageurl, cate, param  
```
使用方法：
```
方法 1：{assign name="list" value=":sp_article(13)"}  
说明：获取 columnid=13 所有文章

方法 2：{assign name="list" value=":sp_article(13, 8)"}  
说明：获取 columnid=13 and cateid=8 所有文章

方法 3：{assign name="list" value=":sp_article(13, 8, 10)"}  
说明：获取 columnid=13 and cateid=8 前 10 条文章

方法 4：{assign name="list" value=":sp_article(13, 0, 10)"}  
说明：获取 columnid=13 前 10 条文章
```

使用 php 直接调用：
```
方法 1：<?php $list = sp_article(13); ?>  
方法 2：<?php $list = sp_article(13, 8); ?>  
方法 3：<?php $list = sp_article(13, 8, 10); ?>  
方法 4：<?php $list = sp_article(13, 0, 10); ?>  
```

调用自定义参数：
```
案例 1：
<?php
    // $options 数组的内容都是选填。
    $options = [
        // 定义 where 参数后，$columnid, $cateid 这二个参数将会自动失效
        'where' =>  'wsarticle_columnid = 10 and find_in_set(5, wsarticle_cateid)',
        // 排序方式 不填写按默认方式排序
        'order' => 'wscate_sort asc',
        // 不设置该参数将不分页
        'pageurl' => '/news-p[page]',
        // 分类信息，true 时可以在列表中调用分类信息
        'cate' => false,
        // 自定义信息，true 时可以在列表中调用自定义字段
        'param' => false,
    ];
    // 获取 columnid = 10 and cateid = 5 的数据，每页 10 条数据。
    // 生成的 url 为 /news-p1.html，/news-p2.html，/news-p3.html
    $list = sp_article(0, 0, 10, $options);
?>
案例 2：演示使用 where 参数 不分页
<?php
    $options = [
        'where' =>  'wsarticle_columnid = 10 and find_in_set(5, wsarticle_cateid)',
        'order' => 'wscate_sort asc',
    ];
    // 获取 columnid = 10 and cateid = 5 的所有数据。
    $list = sp_article(0, 0, 0, $options);
?>
案例 3：演示栏目分页
<?php
    $options = [
        'pageurl' => '/news-p[page]', // 分页 url 地址 [page] 为必须
    ];
    $list = sp_article(10, 0, 12, $options);
    
    // 或者简化成以下方式调用
    $list = sp_article(10, 0, 12, ['pageurl'=>'/news-p[page]']);
?>
案例 4：演示栏目分类分页
<?php
    $options = [
        'pageurl' => '/news/industry-news-5-p[page]', // 分页 url 地址 [page] 为必须
    ];
    // 获取 columnid = 10 and cateid = 5 的数据，每页 10 条数据。
    // 生成的 url 为 /news/industry-news-5-p1.html，/news/industry-news-5-p2.html
    $list = sp_article(10, 5, 10, $options);
?>
案例 5：搜索分页功能
<?php
    $keywords = '关键词';
    $options = [
        'where' => "wsarticle_columnid = 10 and wsarticle_title like '%".$keywords."%'",
        'pageurl' => '/news/industry-news-5-p[page]', // 分页 url 地址 [page] 为必须
    ];
    // 获取 columnid = 10 and cateid = 5 的数据，每页 10 条数据。
    // 生成的 url 为 /news/industry-news-5-p1.html，/news/industry-news-5-p2.html
    $list = sp_article(10, 5, 10, $options);
?>
```

##### sp_list($where, $order, $pagesize, $pageurl, $options) 文章列表
参数说明：
```
$where 查询条件 (必填)
$order 排序方式 (选填)
$pagesize 获取数量 / 分页数量 (选填)
$pageurl 分页地址 (选填)
$options 自定义参数 (选填)
```

```
案例 1：最简单的使用方法
<?php 
    // 获取 columnid = 21 的全部文章
    $list = sp_list("wsarticle_columnid = 21");
?>

案例 2：分页数据
<?php 
    $where = "wsarticle_columnid = 21";
    $order = "wsarticle_create_time desc";
    $pagesize = 10;
    $pageurl = "/news-p[page]";
    $options = ['cate'=>true, 'param'=>true];
    $list = sp_list($where, $order, $pagesize, $pageurl, $options);
?>

案例 3：分类分页数据
<?php 
    $where = "wsarticle_columnid = 21 and find_in_set(5, wsarticle_cateid)";
    $order = "wsarticle_create_time desc";
    $pagesize = 10;
    $pageurl = "/news/industry-5-p[page]";
    $options = ['cate'=>true, 'param'=>true];
    $list = sp_list($where, $order, $pagesize, $pageurl, $options);
?>

案例 4：文章搜索
<?php 
    $keywords = "搜索关键词";
    $where = "wsarticle_columnid = 21 and wsarticle_title like '%".$keywords."%'";
    $order = "wsarticle_create_time desc";
    $pagesize = 10;
    $pageurl = "/news-p[page].html?keywords=".$keywords;
    $options = ['cate'=>true, 'param'=>true];
    $list = sp_list($where, $order, $pagesize, $pageurl, $options);
?>
```



#### 参考(thinkphp5.1)：
> 主页：https://www.kancloud.cn/manual/thinkphp5_1  
> 模板：https://www.kancloud.cn/manual/thinkphp5_1/354069  
> 数据库：https://www.kancloud.cn/manual/thinkphp5_1/353997  

常用标签
> assign 定义标签 https://www.kancloud.cn/manual/thinkphp5_1/354090  
> volist 循环标签 https://www.kancloud.cn/manual/thinkphp5_1/354084  
> 其它内置标签 https://www.kancloud.cn/manual/thinkphp5_1/354083 

